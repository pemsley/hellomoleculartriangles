
#include <pybind11/pybind11.h>
#include <pybind11/embed.h> // everything needed for embedding

// this block is needed before include of display-info.hh
#include <mmdb2/mmdb_manager.h>
#include <math.h>
#include <gtk/gtk.h>
#include <epoxy/gl.h>
#ifndef __APPLE__
#include <epoxy/glx.h>
#endif

#include "display-info.hh"

int add(int i, int j) {
    return i + j;
}

void black() {
   display_info_t di;
   di.bg_col = glm::vec3(0,0,0);
   gtk_widget_queue_draw(di.gl_area);
}

void white() {
   display_info_t di;
   di.bg_col = glm::vec3(1,1,1);
   gtk_widget_queue_draw(di.gl_area);
}

PYBIND11_EMBEDDED_MODULE(hmt, m) {
    m.doc() = "pybind11 hmt plugin"; // optional module docstring

    m.def("add", &add, "A function which adds two numbers");

    m.def("black", &black, "Background black");
    m.def("white", &white, "Background white");

}


PYBIND11_EMBEDDED_MODULE(fast_calc, m) {
    m.def("add", [](int i, int j) {
        return i + j;
    });
}
