
#include <glm/glm.hpp>
#include <gtk/gtk.h>
#include "residue-and-atom-specs.hh"

GtkWidget *do_glarea_demo_window();
void setup_gtk_gl_area_widget(GtkWidget *glarea_box, GtkWidget *box_for_reps);
void on_entry_activate(GtkEntry *entry, gpointer  user_data);

void import_molecule_from_file(const std::string &file_name);

void add_rep_to_representations_table(unsigned int rep_index, const std::string &name);
void add_this_rep_to_representations_table();

void set_draw_ao(bool state);

void set_draw_fog(bool state);

void quick_ribs();

void quick_surf();

void quick_ligands();
void quick_waters();

void test_python();

void black_background();
void white_background();
void grey_background();

void  draw_normals(bool state);

void spin_the_lights();
void import_a_pumpkin();

void quick_ribs();
void quick_surf();
void quick_ligands();
void quick_ligand_neighbours();

void add_H_bond(const coot::atom_spec_t &spec_1,
                const coot::atom_spec_t &spec_2,
                mmdb::Manager *mol);

std::vector<mmdb::Chain *> chains_in_mol(mmdb::Manager *mol);

void add_atom_label(const coot::atom_spec_t &spec,
                    const glm::vec4 &col,
                    mmdb::Manager *mol);


void resize(GtkGLArea *glarea, gint width, gint height);

