
#ifndef DISPLAY_INFO_HH
#define DISPLAY_INFO_HH

#include <vector>
#include <string>
#include <map>
#include <iostream>
#include <fstream>
#include <algorithm>
#include <sys/stat.h>

#include <gtk/gtk.h>
#include <glm/ext.hpp>
#include <mmdb2/mmdb_manager.h>
#include "lights-info.hh"
#include "Mesh.hh"
#include "Model.hh"
#include "Shader.hh"
#include "Texture.hh"
#include "TextureMesh.hh"
#include "framebuffer.hh"
#include "molecular-mesh-generator.hh"

#include "obj_loader.h"

// FreeType
#include <ft2build.h>
#include FT_FREETYPE_H
#include "ft-character.hh"

class atom_label_info_t {
public:
   std::string label;
   glm::vec3 position;
   glm::vec4 colour;
   atom_label_info_t(const std::string &l, const glm::vec3 &p, const glm::vec4 c) :
      label(l), position(p), colour(c) {}
};

class display_info_t {
public:
   display_info_t() { }
   static GtkWidget *main_application;
   static GtkWidget *gl_area;
   static GtkWidget *rep_box; // the vertical box of representation labels
   static glm::vec3 bg_col;
   static bool do_depth_fog;
   static bool do_outline;
   static bool do_normals; // of the last mesh

   static mmdb::Manager *mol;

   static glm::vec3 rotation_centre;
   static std::map<unsigned int, lights_info_t> lights;
   static std::vector<Mesh> meshes; // all these meshes have the same shader
                                    // if you want a different shader, use
                                    // a different vector of meshes.
   static std::vector<TextureMesh> texture_meshes;
   static std::map<std::string, Texture> textures;
   static std::vector<Model> models;


   static std::vector<std::string> command_history;
   static int command_history_index;
   static bool do_ambient_occlusion;

   static framebuffer framebuffer_for_screen;
   static bool use_framebuffer_for_screen;
   static unsigned int framebuffer_scale;

   static std::map<GLchar, FT_character> ft_characters;
   static bool vera_font_loaded;
   void load_freetype_font_textures();

   static void redraw() {
      gtk_widget_queue_draw(gl_area);
   }
   void init_lights();
   void add_to_rotation_centre(const glm::vec3 &delta);

   void resize() {
      gtk_gl_area_attach_buffers(GTK_GL_AREA(gl_area));
      GtkAllocation allocation;
      gtk_widget_get_allocation(GTK_WIDGET(gl_area), &allocation);
      int width  = allocation.width;
      int height = allocation.height;
      unsigned int index_offset = 0;
      if (display_info_t::use_framebuffer_for_screen) {
         if (true)
            std::cout << "calling init on framebuffer_for_screen "
                      << "scale " << framebuffer_scale << " " 
                      << " gl_area " << " " << gl_area
                      << " " << width << "x" << height << " " << std::endl;
         framebuffer_for_screen.init(framebuffer_scale * width,
                                     framebuffer_scale * height,
                                     index_offset, "screen/occlusion");
      }
   }

   float get_screen_ratio() const {
      GtkAllocation allocation;
      gtk_widget_get_allocation(gl_area, &allocation);
      float w = static_cast<float>(allocation.width);
      float h = static_cast<float>(allocation.height);
      float screen_ratio = static_cast<float>(w)/static_cast<float>(h);
      return screen_ratio;
   }
   int get_width() const {
      GtkAllocation allocation;
      gtk_widget_get_allocation(gl_area, &allocation);
      return allocation.width;
   }
   int get_height() const {
      GtkAllocation allocation;
      gtk_widget_get_allocation(gl_area, &allocation);
      return allocation.height;
   }
   static void attach_buffers() {
      gtk_gl_area_attach_buffers(GTK_GL_AREA(gl_area));
   }
   void add_to_history(const std::string &s) {
      std::cout << "adding .... " << s << std::endl;
      command_history.push_back(s);
      command_history_index = command_history.size();
   }
   std::string get_previous_command() const {
      if (false) { // debug
         for (unsigned int i=0; i<command_history.size(); i++) {
            std::string c = " ";
            if (i == command_history_index) c = "*";
            std::cout << " " << c << " " << i << " " << command_history[i] << std::endl;
         }
      }
      command_history_index -= 1;
      if (command_history_index < 0) {
         command_history_index = 0;
         return "";
      } else {
         return command_history[command_history_index];
      }
   }
   std::string get_next_command() const {
      command_history_index += 1;
      int idx_max = command_history.size() - 1;
      if (command_history_index > idx_max)
         command_history_index = idx_max;
      return command_history[command_history_index];
   }
   static void write_history() {
      if (! command_history.empty()) {
         std::vector<std::string> all_history;
         std::ifstream fin(".hmt");
         std::string line;
         while (std::getline(fin, line)) {
            all_history.push_back(line);
         }
         all_history.insert(all_history.end(), command_history.begin(), command_history.end());
         std::ofstream f(".hmt");
         std::vector<std::string>::iterator it;
         for (it=command_history.begin(); it!=command_history.end(); ++it) {
            const std::string &hs = *it;
            std::vector<std::string>::iterator it_next = it + 1;
            if (std::find(it_next, command_history.end(), hs) == command_history.end()) {
               f << hs << "\n";
            }
         }
         f.close();
      }
   }
   static void read_history() {
      std::ifstream fin(".hmt");
      std::string line;
      while (std::getline(fin, line)) {
         command_history.push_back(line);
      }
      command_history_index = command_history.size();
   }
   mmdb::Residue *choose_closest_ligand(const std::vector<mmdb::Residue *> &ligs) const {
      mmdb::Residue *r = 0;
      float dist_max = 99999999.9;
      for (unsigned int i=0; i<ligs.size(); i++) {
         mmdb::Residue *residue_p = ligs[i];
         mmdb::Atom **residue_atoms = 0;
         int n_residue_atoms = 0;
         residue_p->GetAtomTable(residue_atoms, n_residue_atoms);
         for (int iat=0; iat<n_residue_atoms; iat++) {
            mmdb::Atom *at = residue_atoms[iat];
            if (! at->isTer()) {
               glm::vec3 p(at->x, at->y, at->z);
               float d = glm::distance(rotation_centre, p);
               if (d < dist_max) {
                  // std::cout << "with d = " << d << " setting " << << std::endl;
                  r = residue_p;
                  dist_max = d;
               }
            }
         }
      }
      return r;
   }

   void spin_the_lights(float angle_frac) {
      glm::vec3 axis(0,1,0); // camera/view space
      std::map<unsigned int, lights_info_t>::iterator it;
      unsigned int light_idx = 0;
      it = lights.find(light_idx);
      if (it != lights.end())
         it->second.rotate_light_direction(angle_frac, axis);
      light_idx = 1;
      if (it != lights.end())
         it->second.rotate_light_direction(angle_frac, axis);
   }

   // eye_position and bg_col can become member data of this class
   //
   void draw_meshes(Shader *shader_p, const glm::mat4 &mvp,
                    const glm::mat4 &quat_mat,
                    const glm::vec3 &eye_position) {

      glm::vec4 bg_col_v4(bg_col, 1.0f);
      for (unsigned int i=0; i<meshes.size(); i++)
         meshes[i].draw(shader_p, mvp, quat_mat, lights, eye_position, bg_col_v4, do_depth_fog);
      if (! meshes.empty())
         if (do_normals)
            meshes.back().draw_normals(mvp, 0.2);
   }

   void draw_models(Shader *shader_for_tmeshes_p, // obj.shader
                    Shader *shader_for_meshes_p,
                    const glm::mat4 &mvp,
                    const glm::mat4 &quat_mat, const glm::vec3 &eye_position) {

      glm::vec4 bg_col_v4(bg_col, 1.0f);
      for (unsigned int ii=0; ii<models.size(); ii++) {
         models[ii].draw_tmeshes(shader_for_tmeshes_p, mvp, quat_mat, lights, eye_position, bg_col_v4, do_depth_fog);
         models[ii].draw_meshes( shader_for_meshes_p,  mvp, quat_mat, lights, eye_position, bg_col_v4, do_depth_fog);
      }
   }

   static std::vector<atom_label_info_t> atom_labels;
   void add_atom_label(const std::string &l, const glm::vec3 &p, const glm::vec4 &c) {
      atom_labels.push_back(atom_label_info_t(l, p, c));
   }

   void draw_atom_labels(TextureMesh &tmesh,
                         Shader *shader_p,
                         const glm::mat4 &mvp,
                         const glm::mat4 &quat_mat,
                         const glm::vec3 &eye_position,
                         const glm::vec4 &bg_col) {
      if (! atom_labels.empty()) {
         for (unsigned int i=0; i<atom_labels.size(); i++) {
            const std::string &label  = atom_labels[i].label;
            const glm::vec3 &position = atom_labels[i].position;
            const glm::vec4 &colour   = atom_labels[i].colour;
            bool is_perspective_projection = true;
            tmesh.draw_atom_label(label, position, colour, shader_p,
                                  mvp, quat_mat, lights, eye_position, bg_col, do_depth_fog, is_perspective_projection);
         }
      }
   }

   void draw_texture_meshes(Shader *shader_p,
                            const glm::mat4 &mvp,
                            const glm::mat4 &quat_mat,
                            const glm::vec3 &eye_position,
                            const glm::vec4 &bg_col) {
      for (unsigned int i=0; i<texture_meshes.size(); i++)
         texture_meshes[i].draw(shader_p, mvp, quat_mat, lights, eye_position, bg_col, do_depth_fog);
   }

   void add_mesh(const Mesh &mesh_in) {
      meshes.push_back(mesh_in);
   }

   void add_texturemesh(const TextureMesh &mesh_in) {
      texture_meshes.push_back(mesh_in);
   }

   void add_model(const Model &model) {
      models.push_back(model);
   }

   void add_model(const std::vector<molecular_triangles_mesh_t> &mtm,
                  Shader *shader_p, Material material) {
      Model m(mtm, shader_p, material, gl_area);
      models.push_back(m);
   }

   bool write_model_as_obj(unsigned int idx, const std::string &file_name);

   void add_texture(const std::string &name, const std::string &file_name) {
      bool file_exists = true;
      struct stat buffer;
      if (stat (file_name.c_str(), &buffer) != 0) file_exists = false;
      if (! file_exists) {
         std::cout << "failed to find texture image file " << file_name << std::endl;
      } else {
         // happy path
         // we don't want to invoke the destrutor!
         textures[name] = Texture(file_name);
      }

   }

   void set_framebufffer_scale(unsigned int s) {
      framebuffer_scale = s;
      resize();
   }


};

#endif // DISPLAY_INFO_HH
