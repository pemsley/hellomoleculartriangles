
#shader vertex

#version 330 core
layout (location = 0) in vec2 aPos;
layout (location = 1) in vec2 aTexCoords;

out vec2 TexCoords;

void main() {

   TexCoords = aTexCoords;
   gl_Position = vec4(aPos.x, aPos.y, 0.0, 1.0);

}


#shader fragment

#version 330 core

in vec2 TexCoords;

uniform bool do_ambient_occlusion;
uniform bool do_outline;
uniform sampler2D screenTexture;
uniform sampler2D screenDepth;

layout(location = 0) out vec4 out_color;

// this should be turned into a floating point scaling factor
// between 0 and 1 rather than a boolean. "edgyness"
bool too_edgey(float depth_centre, float depth_neighb) {

   float crit_delta = 0.2;
   // when depth_centre is deeper, then crit_delta should be less
   crit_delta = 0.2 - depth_centre * 0.1;
   bool r = ((depth_neighb - depth_centre) > crit_delta);
   if (! r)
      if (depth_neighb == 1.0)
         r = true;
   return r;

}

float rand(vec2 co) {
    return fract(sin(dot(co.xy ,vec2(12.9898,78.233))) * 43758.5453);
}

vec3 occlude() {

   vec3 r = vec3(0.0, 0.0, 0.2); // return r;
   bool do_long_scale = true;
   int n_pixels_max = 36;
   // most of the image:
   int n_sampled = 0;
   int n_closer_neighbours = 0; // ambient occlusion (testing)
   vec2 tex_scale = 1.0/textureSize(screenTexture, 0); // the size of single texel
   float depth_centre = texture(screenDepth, TexCoords).x;
   float closer_sum = 0.0;
   float bias = 0.01;

   bool enable_ao = true; // set this to false if we are on an edge
   for (int ix=0; ix<n_pixels_max; ix++) {
      if (! enable_ao) break;
      for (int iy=0; iy<n_pixels_max; iy++) {
         if ((abs(ix)+abs(iy))> n_pixels_max) continue;
         if (! enable_ao) break;
         // deal with double counting at some stage
         if (true) {
            {
               vec2 offset_coords = TexCoords + vec2(tex_scale.x * ix, tex_scale.y * iy);
               float depth_this = texture(screenDepth, offset_coords).x;
               if (ix == 1 || ix == -1 || iy == 1 || iy == -1)
                  if (too_edgey(depth_centre, depth_this))
                     enable_ao = false;
               n_sampled++;
               if ((depth_this + bias) < depth_centre) {
                  n_closer_neighbours++;
                  closer_sum += depth_this - depth_centre;
               }
            }
            {
               vec2 offset_coords = TexCoords + vec2(tex_scale.x * ix, -tex_scale.y * iy);
               float depth_this = texture(screenDepth, offset_coords).x;
               if (ix == 1 || ix == -1 || iy == 1 || iy == -1)
                  if (too_edgey(depth_centre, depth_this))
                     enable_ao = false;
               n_sampled++;
               if ((depth_this + bias) < depth_centre) {
                  n_closer_neighbours++;
                  closer_sum += depth_this - depth_centre;
               }
            }
            {
               vec2 offset_coords = TexCoords + vec2(-tex_scale.x * ix, tex_scale.y * iy);
               float depth_this = texture(screenDepth, offset_coords).x;
               if (ix == 1 || ix == -1 || iy == 1 || iy == -1)
                  if (too_edgey(depth_centre, depth_this))
                     enable_ao = false;
               n_sampled++;
               if ((depth_this + bias) < depth_centre) {
                  n_closer_neighbours++;
                  closer_sum += depth_this - depth_centre;
               }
            }
            {
               vec2 offset_coords = TexCoords + vec2(-tex_scale.x * ix, -tex_scale * iy);
               float depth_this = texture(screenDepth, offset_coords).x;
               if (ix == 1 || ix == -1 || iy == 1 || iy == -1)
                  if (too_edgey(depth_centre, depth_this))
                     enable_ao = false;
               n_sampled++;
               if ((depth_this + bias) < depth_centre) {
                  n_closer_neighbours++;
                  closer_sum += depth_this - depth_centre;
               }
            }
         }
      }
   }

   float scale_from_long = 1.0;
   int n_long_scale_samples = 100;
   int n_long_scale_samples_per_edge = 10;
   int n_long_scale_count = 0;
   if (do_long_scale) {
      // 0.028 gives a nice/interesting/strange "outliney" look to zoomed out density
      float long_scale = 6.48; // what is the coordinates system?
      bool found_one = false;
      for(int i=0; i<n_long_scale_samples_per_edge; i++) {
         for(int j=0; j<n_long_scale_samples_per_edge; j++) {
            float r_1 = (i - 5) * 0.2;
            float r_2 = (j - 5) * 0.2;
            vec2 offset_coords = vec2(long_scale * tex_scale.x * r_1, long_scale * tex_scale.y * r_2);
            vec2 offseted_coords = TexCoords + offset_coords;
            float depth_sampled = texture(screenDepth, offseted_coords).x;
            if ((depth_sampled+bias) < depth_centre)
               n_long_scale_count++;
         }
      }
      scale_from_long = 1.0 - 0.9 * float(n_long_scale_count)/float(n_long_scale_samples); // was 0.5
      scale_from_long = clamp(scale_from_long, 0.0f, 1.0f);
   }
   if (! enable_ao) {
      r = texture(screenTexture, TexCoords).rgb;
   } else {
      if (n_sampled > 1) {
         r = texture(screenTexture, TexCoords).rgb;
         if ((2 * n_closer_neighbours) >= n_sampled) {
            float aos = float(n_closer_neighbours)/float(n_sampled);
            aos = float(n_closer_neighbours)/float(n_sampled); // 0.5 to 1
            float f = 2.0 * aos - 1.0; // 0.0 to 1.0 (very occluded to no occluded)
            float ff = 1.0 - f * 0.7; // was 0.7

            // Here is where we actually scale down the colour. meh. I don't like it. It's not working well.
            //
            r *= ff;
            r *= scale_from_long;

            // sanity checks
            // if (scale_from_long == 0.0) r = vec3(1.0, 0.0, 0.0);
            // if (n_closer_neighbours > 1)
            //  r = vec3(0.2, 0.2, 0.9);

         }
      } else{
         r = vec3(0.0, 1.0, 0.0);
      }
   }
   return r;
}

// amount goes between 0 and 1, High amount means high degree of original colour.
// To desaturate to grey, amount is 0.1 or so.
vec3
desaturate(vec3 rgb, float amount) {
   // Algorithm from Chapter 16 of OpenGL Shading Language
   const vec3 W = vec3(0.2125, 0.7154, 0.0721);
   vec3 intensity = vec3(dot(rgb, W));
   return mix(intensity, rgb, amount);
}

vec3 some_grey(vec3 col) {

   float amp = col.r + col.g + col.b;
   float r_1 = rand(TexCoords);
   float a = 0.4 * amp * 0.5 * ((1.0 - TexCoords.x) + TexCoords.y) + 0.02 * r_1;
   return vec3(a, a, a * 1.09);

}

vec3 make_outline() {
   int n_pix = 2;

   float depth_centre = texture(screenDepth, TexCoords).x;
   vec3 orig_colour = texture(screenTexture, TexCoords).rgb; // don't blur
   vec3 result = orig_colour; // update this as needed.
   vec2 tex_scale = 1.0/textureSize(screenTexture, 0);
   int n_deep_neighbs = 0;
   for (int ix= -n_pix; ix<=n_pix; ix++) {
      for (int iy= -n_pix; iy<=n_pix; iy++) {
         vec2 offset_coords = TexCoords + vec2(tex_scale.x * ix, tex_scale.y * iy);
         float depth_ij = texture(screenDepth, offset_coords).x;
         if ((depth_ij - depth_centre) > 0.1) {
            n_deep_neighbs++;
         }
      }
   }
   if (n_deep_neighbs > 1) {
      result = vec3(0.1, 0.1, 0.1);
   }
   return result;
}

vec3 blur_edges() {

   vec3 result = texture(screenTexture, TexCoords).rgb;

   float depth_centre = texture(screenDepth, TexCoords).x;
   vec2 tex_scale = 1.0/textureSize(screenTexture, 0); // the size of single texel
   float sum_weight = 0;
   vec3 sum_col = vec3(0,0,0);
   bool on_an_edge = false;
   for (int ix=-1; ix<2; ix++) {
      for (int iy=-1; iy<2; iy++) {
         vec2 offset_coords = TexCoords + vec2(tex_scale.x * ix, tex_scale.y * iy);
         float depth_this = texture(screenDepth, offset_coords).x;
         float delta_depth = abs(depth_this - depth_centre);
         if (delta_depth > 0.1) {
            on_an_edge = true;
            break;
         }
      }
   }
   if (on_an_edge) {
      for (int ix=-1; ix<2; ix++) {
         for (int iy=-1; iy<2; iy++) {
            vec2 offset_coords = TexCoords + vec2(tex_scale.x * ix, tex_scale.y * iy);
            float depth_this = texture(screenDepth, offset_coords).x;
            float delta_depth = abs(depth_this - depth_centre);
            float w = 1.0;
            sum_weight += w;
            sum_col += w * texture(screenTexture, offset_coords).rgb;
         }
      }
   }
   if (sum_weight > 0) {
      vec3 current_colour = texture(screenTexture, TexCoords).rgb;
      float w_centre = 0.1;
      sum_col += w_centre * current_colour;
      sum_weight += w_centre;
      float s = 1.0 / sum_weight;
      result = s * sum_col;
   } else {
      vec3 current_colour = texture(screenTexture, TexCoords).rgb;
      return current_colour;
   }

   return result;

}

void main() {

   bool do_blur_edges = false; // it blurs. Not a good look.

   vec3 result = vec3(0,0,0);

   float depth = texture(screenDepth,   TexCoords).x;

   // this line make a difference. If it is not there, we are
   // editing the whole texture. If it is there, we are editing
   // only the pixels of the object (not the background).
   //
   // gl_FragDepth = depth; // needed for depth for next shader

   if (do_outline) {
      result = make_outline();
      if (depth == 1)
         result = texture(screenTexture, TexCoords).rgb;
   } else {

      if (do_ambient_occlusion) {
         if (depth == 1) {
            // just the background colour
            result = texture(screenTexture, TexCoords).rgb;
            // result += some_grey(result);
         } else {
            result = occlude();
         }
      } else {

         if (do_blur_edges) {
            result = blur_edges();
         } else {
            if (depth == 1) {
               // just the background colour
               result = texture(screenTexture, TexCoords).rgb;
               // result += some_grey(result);
            } else {
               result = texture(screenTexture, TexCoords).rgb;
            }
         }
      }
   }

   out_color = vec4(desaturate(result, 0.8), 1.0);

   // just check that we are using this framebuffer/shader
   // if (depth == 1.0) out_color = vec4(0,0,1,1);

}

