
#include "utils.hh"

glm::vec3 fcxxcoord_to_glm(FCXXCoord &c) {

   return glm::vec3(c.x(), c.y(), c.z());
}


FCXXCoord glm_to_fcxxcoord(glm::vec3 &c) {

   return FCXXCoord(c.x, c.y, c.z);
}
