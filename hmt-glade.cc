
#include <string>
#include <iostream>

// this block is needed before include of display-info.hh
#include <mmdb2/mmdb_manager.h>
#include <math.h>
#include <gtk/gtk.h>
#include <epoxy/gl.h>
#ifndef __APPLE__
#include <epoxy/glx.h>
#endif

#include "display-info.hh"

extern std::string molecular_triangles_pdb_file_name;
extern std::string obj_file_name;

void setup_gtk_gl_area_widget(GtkWidget *box_for_glarea, GtkWidget *box_for_reps);

int main(int argc, char **argv) {

   int status = 0;
   mmdb::InitMatType();

   if (argc > 1) {
      if (argc > 2) {
         if (std::string(argv[1]) == "obj") {
            obj_file_name =  argv[2];
         }
      } else {
         molecular_triangles_pdb_file_name = argv[1]; // used in realize()
      }
   }

   gtk_init(&argc, &argv);

   std::string glade_file = "hmt.glade";

   if (! g_file_test(glade_file.c_str(), G_FILE_TEST_EXISTS)) {
      std::cout << "ERROR:: glade file " << glade_file << " does not exist" << std::endl;
      status = 1;

   } else {
      GtkBuilder *builder = gtk_builder_new_from_file(glade_file.c_str());
      if (! builder) {
         std::cout << "Sadness:: no builder from " << glade_file << std::endl;
      } else {

         display_info_t::main_application = GTK_WIDGET(gtk_builder_get_object(builder, "main_application"));
         GtkWidget *application_window = GTK_WIDGET(gtk_builder_get_object(builder, "main_application"));
         GtkWidget *glarea_and_display_control_hbox = GTK_WIDGET(gtk_builder_get_object(builder, "glarea_and_display_control_hbox"));
         GtkWidget *file_chooser = GTK_WIDGET(gtk_builder_get_object(builder, "file_chooser"));
         GtkWidget *rep_box      = GTK_WIDGET(gtk_builder_get_object(builder, "rep_box"));

         g_object_set_data(G_OBJECT(application_window), "file_chooser", file_chooser);
         g_object_set_data(G_OBJECT(application_window), "rep_box", rep_box);

         if (application_window) {

            GtkWidget *w = 0; // what is this?
            gtk_builder_connect_signals(builder, application_window);

            setup_gtk_gl_area_widget(glarea_and_display_control_hbox, rep_box);
            gtk_widget_show_all(application_window);
            display_info_t::read_history();
         } else {
            std::cout << "ERROR:: failed to get application window" << std::endl;
         }

         g_object_unref(G_OBJECT (builder));
      }
      gtk_main ();
   }

   return status;
}

