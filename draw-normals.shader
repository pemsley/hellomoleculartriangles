
#shader vertex
#version 330 core

layout(location = 0) in vec3 position;
layout(location = 1) in vec4 colour;
uniform mat4 mvp;

out vec4 colour_transfer;

void main() {
   colour_transfer = colour;
   gl_Position = mvp * vec4(position, 1.0);
}

#shader fragment
#version 330 core
in vec4 colour_transfer;
out vec4 output_colour;
void main() {

   output_colour = colour_transfer;

}
