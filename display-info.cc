//
// rework the headers so that I don't need to include epoxy here
#include <epoxy/gl.h>
#include "display-info.hh"


void
display_info_t::init_lights() {

   lights_info_t light;
   light.is_on = true;
   light.direction = glm::vec4(glm::normalize(glm::vec3(-1.0f, 0.0f, 1.0f)), 1.0f);
   lights[0] = light; // a map

   light.direction = glm::vec4(glm::normalize(glm::vec3(1.0f, 0.0f, 0.3f)), 1.0f);
   light.ambient  = glm::vec4(0.3, 0.3, 0.3, 1.0);
   light.diffuse  = glm::vec4(0.3, 0.3, 0.3, 1.0);
   light.specular = glm::vec4(0.3, 0.3, 0.3, 1.0);
   light.is_on = true;
   lights[1] = light;
}

void
display_info_t::load_freetype_font_textures() {

   // ----------------------------- font test -----------------
   FT_Library ft;
   if (FT_Init_FreeType(&ft))
      std::cout << "ERROR::FREETYPE: Could not init FreeType Library" << std::endl;

   FT_Face face;
   // std::string pkgdatadir = coot::package_data_dir();
   // std::string font_dir  = coot::util::append_dir_dir(pkgdatadir, "fonts");
   //std::string font_path = coot::util::append_dir_file(font_dir, "Vera.ttf");
   // std::string font_path = "fonts/LondonBetween.ttf";
   std::string font_path = "fonts/AbakuTLSymSans-Regular.ttf";

   // if (! coot::file_exists(font_path))
   //    font_path = "fonts/Vera.ttf";

   if (FT_New_Face(ft, font_path.c_str(), 0, &face)) {
      std::cout << "ERROR::FREETYPE: Failed to load font" << std::endl;
   } else {
      vera_font_loaded = true;
      // FT_Set_Pixel_Sizes(face, 0, 24); too big for labels
      // FT_Set_Pixel_Sizes(face, 0, 16);
      FT_Set_Pixel_Sizes(face, 0, 40);

      glPixelStorei(GL_UNPACK_ALIGNMENT, 1); // Disable byte-alignment restriction
      // only using one byte.

      for (GLubyte ic = 0; ic < 128; ic++) {
         // Load character glyph
         if (FT_Load_Char(face, ic, FT_LOAD_RENDER)) {
            std::cout << "ERROR::FREETYTPE: Failed to load Glyph" << std::endl;
            continue;
         }
         // Generate texture
         GLuint texture;
         glGenTextures(1, &texture);
         GLenum err = glGetError();
         if (err) std::cout << "Loading characture textures glGenTextures err " << err << std::endl;
         glBindTexture(GL_TEXTURE_2D, texture);
         err = glGetError();
         if (err) std::cout << "Loading characture textures glBindTexture err " << err << std::endl;
         glTexImage2D( GL_TEXTURE_2D,
                       0,
                       GL_RED,
                       face->glyph->bitmap.width,
                       face->glyph->bitmap.rows,
                       0,
                       GL_RED,
                       GL_UNSIGNED_BYTE,
                       face->glyph->bitmap.buffer);
         // Set texture options
         err = glGetError(); if (err) std::cout << "Loading characture textures glTexImage2D err "
                                                << err << std::endl;
         glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
         glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
         glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
         glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
         err = glGetError(); if (err) std::cout << "Loading characture textures glTexParameteri err "
                                                << err << std::endl;

         // std::cout << "Storing characture with texture id " << texture << std::endl;
         // Now store the character
         GLuint face_glyph_advance_x = face->glyph->advance.x;
         FT_character character = {
                                   texture,
                                   glm::ivec2(face->glyph->bitmap.width, face->glyph->bitmap.rows),
                                   glm::ivec2(face->glyph->bitmap_left, face->glyph->bitmap_top),
                                   face_glyph_advance_x
         };
         ft_characters[ic] = character; // ic type change
      }
      glBindTexture(GL_TEXTURE_2D, 0);
      // Destroy FreeType once we're finished
      FT_Done_Face(face);
      FT_Done_FreeType(ft);
   }
}



bool
display_info_t::write_model_as_obj(unsigned int idx, const std::string &file_name) {

   int status = false;

   if (idx < models.size()) {
      std::cout << "debug:: writing model " << idx << " as obj " << file_name << std::endl;
      const Model &m = models[idx];
      m.export_as_obj(file_name);
   } else {
      std::cout << "wrong model index " << idx << " of " << models.size() << std::endl;
   }

   return status;
}
 
