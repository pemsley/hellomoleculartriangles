

#include <string>

#include <mmdb2/mmdb_manager.h>

#include <gtk/gtk.h>
#include <epoxy/gl.h>
#ifndef __APPLE__
#include <epoxy/glx.h>
#endif


#include "hmt.hh"
#include "display-info.hh"

GtkWidget *demo_window = 0;
extern std::string molecular_triangles_pdb_file_name;


enum {
  X_AXIS,
  Y_AXIS,
  Z_AXIS,

  N_AXIS
};

extern float rotation_angles[N_AXIS];


static void
on_axis_value_change (GtkAdjustment *adjustment,
                      gpointer       data)
{
  int axis = GPOINTER_TO_INT (data);
  g_assert (axis >= 0 && axis < N_AXIS);
  rotation_angles[axis] = 2.0 * gtk_adjustment_get_value (adjustment);
  display_info_t di;
  gtk_widget_queue_draw (di.gl_area);
}


static void
close_window (GtkWidget *widget)
{

   demo_window = NULL;

   rotation_angles[X_AXIS] = 0.0;
   rotation_angles[Y_AXIS] = 0.0;
   rotation_angles[Z_AXIS] = 0.0;
}

static GtkWidget *
create_axis_slider (int axis)
{
  GtkWidget *box, *label, *slider;
  GtkAdjustment *adj;
  const char *text;

  box = gtk_box_new (GTK_ORIENTATION_HORIZONTAL, 0);

  switch (axis)
    {
    case X_AXIS:
      text = "X axis";
      break;

    case Y_AXIS:
      text = "Y axis";
      break;

    case Z_AXIS:
      text = "Z axis";
      break;

    default:
      g_assert_not_reached ();
    }

  label = gtk_label_new (text);
  gtk_container_add (GTK_CONTAINER (box), label);
  gtk_widget_show (label);

  adj = gtk_adjustment_new (0.0, 0.0, 360.0, 1.0, 12.0, 0.0);
  g_signal_connect (adj, "value-changed",
                    G_CALLBACK (on_axis_value_change),
                    GINT_TO_POINTER (axis));
  slider = gtk_scale_new (GTK_ORIENTATION_HORIZONTAL, adj);
  gtk_container_add (GTK_CONTAINER (box), slider);
  gtk_widget_set_hexpand (slider, TRUE);
  gtk_widget_show (slider);

  gtk_widget_show (box);

  return box;
}

void
on_ao_togglebutton_toggled(GtkToggleButton *togglebutton,
                           gpointer         user_data) {

   if (gtk_toggle_button_get_active(togglebutton))
      set_draw_ao(true);
   else
      set_draw_ao(false);
}

GtkWidget *
create_glarea_window () {


   GtkWidget *window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
   demo_window = window;

   // gtk_window_set_screen (GTK_WINDOW (window), gtk_widget_get_screen (do_widget));

   gtk_window_set_title (GTK_WINDOW (window), "Hello MoleculesToTriangles");
   gtk_window_set_default_size (GTK_WINDOW (window), 400, 600);
   gtk_container_set_border_width (GTK_CONTAINER (window), 12);
   g_signal_connect (window, "destroy", G_CALLBACK (close_window), NULL);

   GtkWidget *vbox = gtk_box_new (GTK_ORIENTATION_VERTICAL, FALSE);
   GtkWidget *hbox = gtk_box_new (GTK_ORIENTATION_HORIZONTAL, FALSE);

   gtk_box_set_spacing (GTK_BOX (vbox), 6);
   gtk_container_add (GTK_CONTAINER (window), vbox);
   gtk_box_pack_start(GTK_BOX(vbox), hbox, TRUE, TRUE, 2);
   gtk_widget_show(hbox);

   GtkWidget *box_for_reps = gtk_box_new(GTK_ORIENTATION_VERTICAL, FALSE);
   gtk_box_pack_start(GTK_BOX(hbox), box_for_reps, TRUE, TRUE, 2);

   // box_for_reps is passed so that it can be added to display_info_t
   setup_gtk_gl_area_widget(hbox, box_for_reps);

   GtkWidget *controls_vbox = gtk_box_new (GTK_ORIENTATION_VERTICAL, FALSE);
   gtk_container_add (GTK_CONTAINER (vbox), controls_vbox);
   gtk_widget_set_hexpand (controls_vbox, TRUE);

   bool do_axis_sliders = false; 
   if (do_axis_sliders)
      for (int i = 0; i < N_AXIS; i++)
         gtk_container_add (GTK_CONTAINER (controls_vbox), create_axis_slider (i));

   GtkWidget *graphics_controls_hbox = gtk_box_new (GTK_ORIENTATION_HORIZONTAL, FALSE);
   gtk_container_add(GTK_CONTAINER(vbox), graphics_controls_hbox);

   GtkWidget *test_python_button = gtk_button_new_with_label("Test Python");
   g_signal_connect (test_python_button, "clicked", G_CALLBACK(test_python), window);
   gtk_box_pack_start(GTK_BOX(graphics_controls_hbox), test_python_button, TRUE, TRUE, 6);

   GtkWidget *black_background_button = gtk_button_new_with_label("Black Background");
   g_signal_connect (black_background_button, "clicked", G_CALLBACK(black_background), window);
   gtk_box_pack_start(GTK_BOX(graphics_controls_hbox), black_background_button, TRUE, TRUE, 6);

   GtkWidget *white_background_button = gtk_button_new_with_label("White Background");
   g_signal_connect (white_background_button, "clicked", G_CALLBACK(white_background), window);
   gtk_box_pack_start(GTK_BOX(graphics_controls_hbox), white_background_button, TRUE, TRUE, 6);

   GtkWidget *grey_background_button = gtk_button_new_with_label("Grey Background");
   g_signal_connect (grey_background_button, "clicked", G_CALLBACK(grey_background), window);
   gtk_box_pack_start(GTK_BOX(graphics_controls_hbox), grey_background_button, TRUE, TRUE, 6);

   GtkWidget *ao_button = gtk_toggle_button_new_with_label("AO");
   g_signal_connect (ao_button, "toggled", G_CALLBACK(on_ao_togglebutton_toggled), window);
   gtk_box_pack_start(GTK_BOX(graphics_controls_hbox), ao_button, TRUE, TRUE, 6);

   GtkWidget *python_hbox = gtk_box_new (GTK_ORIENTATION_HORIZONTAL, FALSE);
   gtk_container_add(GTK_CONTAINER(vbox), python_hbox);
   GtkWidget *label = gtk_label_new("Cmd: ");
   gtk_container_add(GTK_CONTAINER(python_hbox), label);

   GtkWidget *entry = gtk_entry_new();
   g_signal_connect(entry, "activate", G_CALLBACK(on_entry_activate), NULL);
   gtk_box_pack_start(GTK_BOX(python_hbox), entry, TRUE, TRUE, 6);

   GtkWidget *quit_button = gtk_button_new_with_label ("Quit");
   gtk_widget_set_hexpand (quit_button, FALSE);
   gtk_container_add (GTK_CONTAINER (vbox), quit_button);
   g_signal_connect (quit_button, "clicked", G_CALLBACK (gtk_main_quit), window);

   return window;
}


GtkWidget*
do_glarea_demo_window() {

   GtkWidget *window = create_glarea_window();

   if (!gtk_widget_get_visible(demo_window))
      gtk_widget_show_all(demo_window);
   else
      gtk_widget_destroy (demo_window);

   return demo_window;
}


int main(int argc, char **argv) {

  int r = 0;

  gtk_init(&argc, &argv);

  if (argc > 1)
     molecular_triangles_pdb_file_name = argv[1]; // used in realize()

  do_glarea_demo_window();

  gtk_main ();
  return r;
}
