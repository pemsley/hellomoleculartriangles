/* OpenGL Area
 *
 * GtkGLArea is a widget that allows custom drawing using OpenGL calls.
 */

#include <string>
#include <fstream>
#include <iostream>
#include <iomanip>
#include <stdexcept>
#include <sstream>  // needed?
#include <memory>
#include <chrono>

#include <pybind11/pybind11.h>
namespace py = pybind11;

#include <pybind11/embed.h> // everything needed for embedding

// mmdb
#include <mmdb2/mmdb_manager.h>

#include <math.h>
#include <gtk/gtk.h>
#include <epoxy/gl.h>
#ifndef __APPLE__
#include <epoxy/glx.h>
#endif

// GLM
#include <glm/ext.hpp>
#define GLM_ENABLE_EXPERIMENTAL // # for norm things
#include <glm/gtx/string_cast.hpp>  // to_string()
#include <glm/gtx/rotate_vector.hpp> // for orientation

#include <MoleculesToTriangles/CXXClasses/MolecularRepresentation.h>
#include <MoleculesToTriangles/CXXClasses/MolecularRepresentationInstance.h>

#include "Mesh.hh"
#include "TextureMesh.hh"
#include "Texture.hh"
#include "Particle.hh"
#include "Shader.hh" // real coot code
#include "lights-info.hh"
#include "molecular-mesh-generator.hh"

#include "mouse-button-info.hh" // contains accumulating mouse-based orientation quaternion
#include "oct.hh"
#include "hmt.hh"
#include "parse.hh"

#include "screendump-tga.hh"

// unsigned int framebuffer_scale = 2; 

enum {
  X_AXIS,
  Y_AXIS,
  Z_AXIS,

  N_AXIS
};

/* Rotation angles on each axis */
float rotation_angles[N_AXIS] = { 0.0 };

Mesh mesh_for_2_triangles; // contains vertices, triangle indices, as setup function and a draw funtion.
Mesh mesh_for_rama_balls;
Mesh mesh_for_instanced_balls;
Mesh mesh_for_instanced_cylinders;
Mesh mesh_for_instanced_octahemispheres;
Mesh mesh_for_cis_peptide;
Mesh mesh_for_octaspheres;
Mesh mesh_for_molecular_triangles;
Mesh mesh_for_particles;
TextureMesh tmesh_for_camera_facing_quad;
TextureMesh tmesh_for_atom_labels;
Texture texture_for_camera_facing_quad;

int n_particles = 150;

particle_container_t particles;

mouse_button_info_t mouse_button_info;
float distance_of_eye_to_rotation_centre = 30.0f; // "zoom"  // was 10 for testing spheres

static GtkWidget *demo_window = NULL;

/* the GtkGLArea widget */
// static GtkWidget *gl_area = NULL;  now in display_info_t

float z_clip_front = 45.0;
float z_clip_back = 300.0; // was 10.0;

Shader hello_shader;
Shader rama_balls_shader;
Shader instanced_balls_shader;
Shader instanced_cylinder_shader;
Shader octaspheres_shader;
Shader camera_facing_quad_shader;
Shader molecular_triangles_shader;
Shader shader_for_screen;
Shader shader_for_atom_labels;
Shader obj_shader;

int spin_tick_id = -1;

#include "display-info.hh"

void my_glarea_add_other_signals_and_events(GtkWidget *glarea); // use a header?

molecular_mesh_generator_t mol_mesh_gen; // the *generator* for inst cylinders and cis peptide. Maybe use a better name?

std::string molecular_triangles_pdb_file_name;
std::string obj_file_name;


void
init_shaders() {

   hello_shader.init("hello.shader",                            Shader::Entity_t::GENERIC_DISPLAY_OBJECT);
   rama_balls_shader.init("rama-balls.shader",                  Shader::Entity_t::MODEL);
   instanced_balls_shader.init("instanced-balls.shader",        Shader::Entity_t::GENERIC_DISPLAY_OBJECT);
   instanced_cylinder_shader.init("instanced-cylinders.shader", Shader::Entity_t::INSTANCED_DISPLAY_OBJECT);
   camera_facing_quad_shader.init("camera-facing-quad.shader",  Shader::Entity_t::MODEL);
   octaspheres_shader.init("octaspheres.shader",                Shader::Entity_t::GENERIC_DISPLAY_OBJECT);
   shader_for_screen.init("screen.shader",                      Shader::Entity_t::SCREEN);
   shader_for_atom_labels.init("atom-label.shader",             Shader::Entity_t::MODEL); // texCoords?
   molecular_triangles_shader.init("moleculestotriangles.shader", Shader::Entity_t::GENERIC_DISPLAY_OBJECT);
   obj_shader.init("obj.shader", Shader::Entity_t::GENERIC_DISPLAY_OBJECT);

}

void
init_textures() {

   // maybe this can go into display_info_t ?

   display_info_t di;
   di.add_texture("white-marble", "white-marble.jpg");
   di.add_texture("dark-marble", "dark-marble.jpg");
   di.add_texture("crystal-colour",     "zip/Crystal_002_SD/Crystal_002_COLOR.jpg");
   di.add_texture("crystal-normal",     "zip/Crystal_002_SD/Crystal_002_NORM.jpg");
   di.add_texture("crystal-occlusion",  "zip/Crystal_002_SD/Crystal_002_OCC.jpg");
   di.add_texture("concrete-colour",     "zip/Concrete_011_SD/Concrete_011_COLOR.jpg");
   di.add_texture("concrete-normal",     "zip/Concrete_011_SD/Concrete_011_NORM.jpg");
   di.add_texture("concrete-occlusion",  "zip/Concrete_011_SD/Concrete_011_OCC.jpg");
   // di.add_texture("abstract-007-colour",     "zip/Abstract_007_SD/Abstract_007_COLOR.jpg");
   // di.add_texture("abstract-007-normal",     "zip/Abstract_007_SD/Abstract_007_NORM.jpg");
   // di.add_texture("abstract-007-occlusion",  "zip/Abstract_007_SD/Abstract_007_OCC.jpg");
   // di.add_texture("abstract-010-colour",     "zip/Abstract_010_SD/Abstract_010_basecolor.jpg");
   // di.add_texture("abstract-010-normal",     "zip/Abstract_010_SD/Abstract_010_normal.jpg");
   // di.add_texture("abstract-010-occlusion",  "zip/Abstract_010_SD/Abstract_010_ambientOcclusion.jpg");
   di.add_texture("coffee-colour",        "zip/Coffee_Grains_001_SD/Coffee_Grains_001_BaseColor.jpg");
   di.add_texture("coffee-normal",        "zip/Coffee_Grains_001_SD/Coffee_Grains_001_Normal.jpg");
   di.add_texture("coffee-occlusion",     "zip/Coffee_Grains_001_SD/Coffee_Grains_001_AmbientOcclusion.jpg");
   di.add_texture("burlap_003-colour",    "zip/Fabric_Burlap_003_SD/Fabric_Burlap_003_basecolor.jpg");
   di.add_texture("burlap_003-normal",    "zip/Fabric_Burlap_003_SD/Fabric_Burlap_003_normal.jpg");
   di.add_texture("burlap_003-occlusion", "zip/Fabric_Burlap_003_SD/Fabric_Burlap_003_ambientOcclusion.jpg");
   di.add_texture("bricks-colour",      "bricks.jpg");
   di.add_texture("bricks-normal",      "bricks-normal-map.jpg");
   di.add_texture("bricks-occlusion",   "bricks-occlusion.jpg");

   if (false) {
      di.add_texture("backpack-diffuse",   "zip/backpack/diffuse.jpg");
      di.add_texture("backpack-occlusion", "zip/backpack/ao.jpg");
      di.add_texture("backpack-normal",    "zip/backpack/normal.png");
      di.add_texture("backpack-roughness", "zip/backpack/roughness.jpg");
      di.add_texture("backpack-specular",  "zip/backpack/specular.jpg");
   }

   texture_for_camera_facing_quad.init("test-label.png");
}

GLuint screen_quad_vertex_array_id;


float quadVertices[] = { // vertex attributes for a quad that fills the entire screen in Normalized Device Coordinates.
      // positions   // texCoords
      -1.0f,  1.0f,  0.0f, 1.0f,
      -1.0f, -1.0f,  0.0f, 0.0f,
       1.0f, -1.0f,  1.0f, 0.0f,

      -1.0f,  1.0f,  0.0f, 1.0f,
       1.0f, -1.0f,  1.0f, 0.0f,
       1.0f,  1.0f,  1.0f, 1.0f
};

void
init_screen_quads() {

   shader_for_screen.Use();
   // screen quad VAO
   unsigned int quadVBO;
   glGenVertexArrays(1, &screen_quad_vertex_array_id);
   glBindVertexArray(screen_quad_vertex_array_id);
   glGenBuffers(1, &quadVBO);
   glBindBuffer(GL_ARRAY_BUFFER, quadVBO);
   glBufferData(GL_ARRAY_BUFFER, sizeof(quadVertices), &quadVertices, GL_STATIC_DRAW);
   glEnableVertexAttribArray(0);
   glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(float), static_cast<void *>(0));
   glEnableVertexAttribArray(1);
   glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(float), reinterpret_cast<void *>(2 * sizeof(float)));
   GLenum err = glGetError();
   if (err) std::cout << "init_screen_quads() err is " << err << std::endl;

}

void
move_to_origin(mmdb::Manager *mol) {

   float sum[3] = { 0,0,0 };
   unsigned int n_atoms = 0;
   for(int imod = 1; imod<=mol->GetNumberOfModels(); imod++) {
      mmdb::Model *model_p = mol->GetModel(imod);
      if (model_p) {
         int n_chains = model_p->GetNumberOfChains();
         for (int ichain=0; ichain<n_chains; ichain++) {
            mmdb::Chain *chain_p = model_p->GetChain(ichain);
            int nres = chain_p->GetNumberOfResidues();
            for (int ires=0; ires<nres; ires++) {
               mmdb::Residue *residue_p = chain_p->GetResidue(ires);
               int n_residue_atoms = residue_p->GetNumberOfAtoms();
               for (int iat=0; iat<n_residue_atoms; iat++) {
                  mmdb::Atom *at = residue_p->GetAtom(iat);
                  if (at) {
                     if (! at->isTer()) {
                        sum[0] += at->x;
                        sum[1] += at->y;
                        sum[2] += at->z;
                        n_atoms++;
                     }
                  }
               }
            }
         }
      }
   }

   if (n_atoms > 0) {
      float sc = 1.0/static_cast<float>(n_atoms);
      float ave[3] = { 0,0,0 };
      for (unsigned int i=0; i<3; i++)
         ave[i] = sum[i] * sc;
      for(int imod = 1; imod<=mol->GetNumberOfModels(); imod++) {
         mmdb::Model *model_p = mol->GetModel(imod);
         if (model_p) {
            int n_chains = model_p->GetNumberOfChains();
            for (int ichain=0; ichain<n_chains; ichain++) {
               mmdb::Chain *chain_p = model_p->GetChain(ichain);
               int nres = chain_p->GetNumberOfResidues();
               for (int ires=0; ires<nres; ires++) {
                  mmdb::Residue *residue_p = chain_p->GetResidue(ires);
                  int n_residue_atoms = residue_p->GetNumberOfAtoms();
                  for (int iat=0; iat<n_residue_atoms; iat++) {
                     mmdb::Atom *at = residue_p->GetAtom(iat);
                     if (at) {
                        if (! at->isTer()) {
                           at->x -= ave[0];
                           at->y -= ave[1];
                           at->z -= ave[2];
                        }
                     }
                  }
               }
            }
         }
      }
   }
}


std::string string_end(const std::string &ss, unsigned int n_chars_max) {

   std::string s = ss;
   unsigned int l = s.length();
   if (l > n_chars_max) {
      unsigned int b = l - n_chars_max;
      s = ss.substr(b, l);
   }
   return s;
}

#include <clipper/core/coords.h>
#include "text-rendering-utils.hh"



void
toggle_draw_representation(GtkWidget *w, gpointer user_data) {

   int rep_no = GPOINTER_TO_INT(user_data);
   if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(w))) {
      display_info_t::models[rep_no].draw_this_model = true;
   } else {
      display_info_t::models[rep_no].draw_this_model = false;
   }
   display_info_t::redraw();
}

void
edit_representation_clicked(GtkWidget *w, gpointer user_data) {

   int rep_no = GPOINTER_TO_INT(user_data);
   std::cout << "Do something rep_no " << rep_no << std::endl;
   display_info_t::redraw();
}

void
add_rep_to_representations_table(unsigned int rep_index, const std::string &name) {

   std::cout << "add_rep_to_representations_table " << rep_index << " " << name << std::endl;
   display_info_t di;
   std::string ss = std::to_string(rep_index) + " " + string_end(name, 10);
   GtkWidget *box = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0);
   GtkWidget *l = gtk_label_new(ss.c_str());
   GtkWidget *b1 = gtk_toggle_button_new_with_label("S");
   GtkWidget *b2 = gtk_button_new_with_label("E");
   gtk_widget_set_tooltip_text(l, name.c_str());
   gtk_box_pack_start(GTK_BOX(box), l,  FALSE, FALSE, 0);
   gtk_box_pack_end(GTK_BOX(box), b2, FALSE, FALSE, 0);
   gtk_box_pack_end(GTK_BOX(box), b1, FALSE, FALSE, 0);
   gtk_box_pack_start(GTK_BOX(di.rep_box), box, FALSE, FALSE, 2);
   gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(b1), TRUE);
   g_signal_connect(b1, "toggled", G_CALLBACK(toggle_draw_representation),  GINT_TO_POINTER(rep_index));
   g_signal_connect(b2, "clicked", G_CALLBACK(edit_representation_clicked), GINT_TO_POINTER(rep_index));
   gtk_widget_show_all(box);
}

void
add_this_rep_to_representations_table() {

   if (! display_info_t::models.empty()) {
      unsigned int idx = display_info_t::models.size() - 1;
      std::string name = "A model has no name";
      add_rep_to_representations_table(idx, name);
   }

}

void
import_molecule_from_file(const std::string &file_name) {

   mmdb::Manager *mol = new mmdb::Manager;
   mmdb::ERROR_CODE err = mol->ReadCoorFile(file_name.c_str());
   if (err == mmdb::Error_Ok) {
      std::cout << "#################### adding mesh..." << std::endl;
      display_info_t::mol = mol;
      move_to_origin(mol);

      display_info_t di;
      gtk_gl_area_attach_buffers(GTK_GL_AREA(di.gl_area));
      std::string model_name = file_name; // it's what we have at the moment.
      std::string colour_scheme = "ColorRamp";
      std::string style = "Ribbon";

      colour_scheme = "Element";
      style = "Cylinders";

      std::string col_selection = "/*/*/*/*[C].*";
      std::string col_colour = "#999955";
      // col_colour = "#c4c45b";
      molecular_mesh_generator_t mmg;
      mmg.add_selection_and_colour(col_selection, col_colour);

      Material material;
      std::vector<molecular_triangles_mesh_t> mtm =
         mmg.get_molecular_triangles_mesh(di.mol, "//", colour_scheme, style);

      if (! mtm.empty()){
         Model model;
         for (unsigned int ii=0; ii<mtm.size(); ii++) {
            std::pair<std::vector<s_generic_vertex>, std::vector<g_triangle> > vp;
            di.attach_buffers();  // not needed, I think.
            vp.first  = mtm[ii].vertices;
            vp.second = mtm[ii].triangles;
            Mesh mm(vp);
            mm.setup(&molecular_triangles_shader, material);
            model.add_mesh(mm);
         }
         di.add_model(model);
      }

      // di.add_model(mtm, &molecular_triangles_shader, material);

      di.redraw();
      if (! di.models.empty()) {
         unsigned int idx = di.models.size() - 1;
         add_rep_to_representations_table(idx, model_name);
      }

   } else {

      std::cout << "mmdb error code " << err << std::endl;
      std::cout << "problem reading file " << file_name << std::endl;
      if (err == 15)
         std::cout << "can't open file " << file_name << std::endl;
   }
}

/* We need to set up our state when we realize the GtkGLArea widget */
void
realize(GtkWidget *widget) {

   gtk_gl_area_make_current (GTK_GL_AREA (widget));
   if (gtk_gl_area_get_error (GTK_GL_AREA (widget)) != NULL)
      return;

   GtkAllocation allocation;
   gtk_widget_get_allocation(widget, &allocation);
   int w = allocation.width;
   int h = allocation.height;

   gtk_gl_area_set_has_depth_buffer(GTK_GL_AREA(widget), TRUE);
   glEnable(GL_DEPTH_TEST);

   // GdkGLContext *context = gtk_gl_area_get_context(GTK_GL_AREA (widget));


   const char *s1 = reinterpret_cast<const char *>(glGetString(GL_VERSION));
   const char *s2 = reinterpret_cast<const char *>(glGetString(GL_SHADING_LANGUAGE_VERSION));
   const char *s3 = reinterpret_cast<const char *>(glGetString(GL_RENDERER));
   const char *s4 = reinterpret_cast<const char *>(glGetString(GL_VENDOR));
   if (s1 && s2 && s3 && s4) {
      std::string ss1(s1);
      std::string ss2(s2);
      std::string ss3(s3);
      std::string ss4(s4);
      std::cout << "INFO:: GL Version:                  " << ss1 << std::endl;
      std::cout << "INFO:: GL Shading Language Version: " << ss2 << std::endl;
      std::cout << "INFO:: GL Renderer:                 " << ss3 << std::endl;
      std::cout << "INFO:: GL Vendor:                   " << ss4 << std::endl;
   } else {
      std::cout << "error:: on_glarea_realize() null from glGetString()" << std::endl;
   }


   display_info_t di;
   di.load_freetype_font_textures();
   di.init_lights();

   init_shaders();

   init_textures();

   init_screen_quads();


   if (di.use_framebuffer_for_screen) {
      unsigned int fb_index_offset = 0;
      di.framebuffer_for_screen.init(di.framebuffer_scale * w, di.framebuffer_scale * h,
                                     fb_index_offset, "screen/occlusion");
   }

   glm::vec4  ambient(0.1f, 0.1f, 0.1f, 1.0f);
   glm::vec4  diffuse(0.6f, 0.2f, 0.1f, 1.0f);
   glm::vec4 specular(0.5f, 0.5f, 0.5f, 1.0f);
   float shininess = 15.0f;
   Material material(ambient, diffuse, specular, shininess);
   Material material_i = material;
   material_i.diffuse = glm::vec4(0.2f, 0.6f, 0.2f, 1.0f);

   Material material_cis_pep = material;
   material_cis_pep.ambient = glm::vec4(0.1, 0.1, 0.1, 1.0);
   material_cis_pep.diffuse = glm::vec4(0.7, 0.7, 0.2, 1.0);

   // split into 2 vectors because that's how they are sent to the graphics card.
   // 2 vector, one for rot/trans/scale and the other for colour
   //

   if (false) { // testing init/draw
      mesh_for_2_triangles.setup_simple_triangles(&hello_shader, material);
      mesh_for_2_triangles.set_name("2-triangles");
   }

   mesh_for_rama_balls.setup_rama_balls(&rama_balls_shader, material);
   mesh_for_instanced_balls.setup_instanced_debugging_objects(&instanced_balls_shader, material_i);

   // these are together - 2 draw meshes for the ball and stick representation of the molecule
   mesh_for_instanced_octahemispheres.set_name("instanced octahemispheres");
   mesh_for_instanced_octahemispheres.setup_instanced_octahemispheres(&instanced_cylinder_shader,
                                                                      material_i,
                                                                      mol_mesh_gen.atom_instance_matrices,
                                                                      mol_mesh_gen.atom_instance_colours);


#if 0  // this doesn't compile after the merge
   mesh_for_instanced_cylinders.setup_instancing_buffer_data(&instanced_cylinder_shader, material_i,
                                                             mol_mesh_gen.bond_instance_matrices,
                                                             mol_mesh_gen.bond_instance_colours);
#endif
   

   // maybe setup should be done in the constructor, or here, in import(), where we also pass shader
   // and material?

   mesh_for_cis_peptide.import(mol_mesh_gen.get_cis_peptides("test.pdb"));
   mesh_for_cis_peptide.setup(&rama_balls_shader, material);

   mesh_for_rama_balls.set_name("rama-balls");
   mesh_for_cis_peptide.set_name("cis-peptide");

   // camera facing quad test
   tmesh_for_camera_facing_quad.setup_camera_facing_quad(&camera_facing_quad_shader, 1.0, 1.0);
   GLenum err = glGetError(); if (err) std::cout << "realize() D err " << err << std::endl;

   // camera facing quad for atom labels;
   tmesh_for_atom_labels.setup_camera_facing_quad(&shader_for_atom_labels, 1.0, 1.0);
   tmesh_for_atom_labels.set_colour(glm::vec4(0.7, 0.7, 0.2, 1.0));

   // octaspheres, just hanging around

   mesh_for_octaspheres.set_name("octaspheres");
   unsigned int num_subdivisions = 3;
   std::pair<std::vector<glm::vec3>, std::vector<g_triangle> > oct =
      tessellate_octasphere(num_subdivisions);
   glm::vec3 offset(1.6, 0.7, 0); //  move it away from the origin
   std::vector<s_generic_vertex> oct_verts(oct.first.size());
   for (unsigned int i=0; i<oct.first.size(); i++) {
      oct_verts[i].pos = 0.4f * oct.first[i] + offset;
      oct_verts[i].normal = oct.first[i];
      float red  = static_cast<float>(i)     / (4 * pow(2.0, 2 * num_subdivisions));
      float blue = static_cast<float>(400-i) / (8 * pow(2.0, 2 * num_subdivisions));
      oct_verts[i].color = glm::vec4(red, 0.3f, blue, 1.0f);
   }
   mesh_for_octaspheres.import(oct_verts, oct.second);
   if (false) { // more balls
      for (unsigned int i=0; i<oct_verts.size(); i++)
         oct_verts[i].pos += glm::vec3(1.0,0,0);
      mesh_for_octaspheres.import(oct_verts, oct.second);
      for (unsigned int i=0; i<oct_verts.size(); i++)
         oct_verts[i].pos += glm::vec3(1,0,0);
      mesh_for_octaspheres.import(oct_verts, oct.second);
   }
   mesh_for_octaspheres.setup(&octaspheres_shader, material_cis_pep);

   // particles

   mesh_for_particles.set_name("mesh-for-particles");

   std::vector<glm::vec3> particle_positions;
   particle_positions.push_back(display_info_t::rotation_centre);
   particles.make_particles(n_particles, particle_positions);
   mesh_for_particles.setup_vertex_and_instancing_buffers_for_particles(particles.size());

   // molecular triangles - test

   if (! molecular_triangles_pdb_file_name.empty()) {
      import_molecule_from_file(molecular_triangles_pdb_file_name);
   }

   if (! obj_file_name.empty()) {
      TextureMesh tmesh;
      OBJModel obj_model(obj_file_name);
      IndexedModel indexed_model = obj_model.ToIndexedModel();
      tmesh.import(indexed_model, 5.0f);
      std::cout << "---------------------- adding texture mesh from file " << obj_file_name << std::endl;
      di.add_texturemesh(tmesh);
   }

   // setup_hud_text(w, h, shader_for_atom_labels, true);

   // easy recentre for debugging twisted trans peptide.
   // add_to_rotation_centre(glm::vec3(-1.380160, -0.419247, -1.735726));

}

float
calc_z_front_real() {
   return 0.5 * distance_of_eye_to_rotation_centre;
}

/* We should tear down the state when unrealizing */
void
unrealize (GtkWidget *widget) {

   gtk_gl_area_make_current (GTK_GL_AREA (widget));

   if (gtk_gl_area_get_error (GTK_GL_AREA (widget)) != NULL)
      return;

   glDeleteBuffers (1, &mesh_for_2_triangles.buffer_id);
   glDeleteBuffers (1, &mesh_for_2_triangles.vao);
   hello_shader.close(); // delete the program
}


glm::vec3 get_eye_position(const glm::mat4 &mouse_quat_mat);
glm::vec3 get_camera_up_direction(const glm::mat4 &mouse_quat_mat);

glm::mat4 get_mvp() {

   display_info_t di;
   glm::mat4 model_matrix(1.0);
   glm::mat4 view_matrix(1.0);

   if (true) {
      model_matrix = glm::rotate(model_matrix, 0.009f * rotation_angles[0], glm::vec3(1,0,0));
      model_matrix = glm::rotate(model_matrix, 0.009f * rotation_angles[1], glm::vec3(0,1,0));
      model_matrix = glm::rotate(model_matrix, 0.009f * rotation_angles[2], glm::vec3(0,0,1));
   }

   glm::mat4 quat_mat(mouse_button_info.quat);
   // model_matrix = quat_mat * model_matrix;
   // model_matrix = glm::translate(model_matrix, rotation_centre);

   double near = -2.0f;
   double far  =  2.0f;
   glm::mat4 proj = glm::ortho(-1.0, 1.0, -1.0, 1.0, near, far);
   glm::mat4 mvp = proj * view_matrix * model_matrix;

   bool use_perspective_projection = true;

   if (use_perspective_projection) {
      float fov = 35.0; // degrees, the smaller this value, the more we seem to be
                        // "zoomed in."
      float screen_ratio = di.get_screen_ratio();
      glm::vec3 ep_ori = get_eye_position(quat_mat);
      glm::vec3 ep = ep_ori + display_info_t::rotation_centre;
      glm::vec3 rc = display_info_t::rotation_centre;
      glm::vec3 up = get_camera_up_direction(quat_mat);
      view_matrix = glm::lookAt(ep, rc, up);
      float z_front_real = calc_z_front_real();
      float z_back_real  = z_front_real + distance_of_eye_to_rotation_centre * 1.6;
      if (false)
         std::cout << "eye distance " << distance_of_eye_to_rotation_centre << " "
                   << z_front_real << " " << z_back_real << std::endl;
      proj = glm::perspective(glm::radians(fov), screen_ratio, z_front_real, z_back_real);
      mvp = proj * view_matrix * model_matrix;
   }

   return mvp;

}

glm::vec3
get_eye_position(const glm::mat4 &mouse_quat_mat) {

   // where does  0,0,z get rotated to when we apply the matrix?

   glm::vec4 z_p(0.0f, 0.0f, distance_of_eye_to_rotation_centre, 1.0f);
   glm::vec4 r = z_p * mouse_quat_mat;
   return glm::vec3(r);

}

glm::vec3
get_camera_up_direction(const glm::mat4 &mouse_quat_mat) {

   glm::vec4 z_p(0.0f, 1.0f, 0.0f, 1.0f);
   glm::vec4 r = z_p * mouse_quat_mat;
   glm::vec3 r3(r);
   return r3;
}

void draw_normals(bool state) {
   display_info_t::do_normals = state;
   display_info_t::redraw();
}


glm::mat4
get_particle_mvp() {

   glm::mat4 model_matrix(1.0);
   glm::mat4 view_matrix(1.0);

   double near = -2.0f;
   double far  =  2.0f;
   glm::mat4 proj = glm::ortho(-1.0, 1.0, -1.0, 1.0, near, far);
   glm::mat4 mvp = proj * view_matrix * model_matrix;

   bool use_perspective_projection = true;

   if (use_perspective_projection) {
      float fov = 35.0; // degrees, the smaller this value, the more we seem to be
                        // "zoomed in."
      float screen_ratio = 1.0;
      float z_front = 0.1;
      float z_back = 160;
      glm::mat4 quat_mat(mouse_button_info.quat);
      glm::vec3 ep_ori = get_eye_position(quat_mat);
      glm::vec3 ep = ep_ori + display_info_t::rotation_centre;
      glm::vec3 rc = display_info_t::rotation_centre;
      glm::vec3 up = get_camera_up_direction(quat_mat);
      view_matrix = glm::lookAt(ep, rc, up);
      for (unsigned int i=0; i<3; i++)
         for (unsigned int j=0; j<3; j++)
            view_matrix[i][j] = 0.0f;
      for (unsigned int j=0; j<3; j++)
         view_matrix[j][j] = 1.0f;
      proj = glm::perspective(glm::radians(fov), screen_ratio, z_front, z_back);
      mvp = proj * view_matrix * model_matrix;
   }

   return mvp;
}


glm::mat4 get_world_rotation_matrix() {

   glm::mat4 model_matrix(1.0);

   model_matrix = glm::rotate(model_matrix, 0.009f * rotation_angles[0], glm::vec3(1,0,0));
   model_matrix = glm::rotate(model_matrix, 0.009f * rotation_angles[1], glm::vec3(0,1,0));
   model_matrix = glm::rotate(model_matrix, 0.009f * rotation_angles[2], glm::vec3(0,0,1));

   // the world no longer rotates - the view does. I don't (yet) account for the above sliders
   //
   // glm::mat4 quat_mat(mouse_button_info.quat);
   // model_matrix = quat_mat * model_matrix;

   return model_matrix;
}

glm::mat4 get_world_rotation_translation_matrix() {

   glm::mat4 model_matrix(1.0);

   if (true) {
      model_matrix = glm::rotate(model_matrix, 0.009f * rotation_angles[0], glm::vec3(1,0,0));
      model_matrix = glm::rotate(model_matrix, 0.009f * rotation_angles[1], glm::vec3(0,1,0));
      model_matrix = glm::rotate(model_matrix, 0.009f * rotation_angles[2], glm::vec3(0,0,1));
   }

   // glm::mat4 quat_mat(mouse_button_info.quat);
   // model_matrix = quat_mat * model_matrix;
   // model_matrix = glm::translate(model_matrix, rotation_centre);

   return model_matrix;
}

glm::mat4 get_view_rotation_matrix() {

  glm::mat4 view_matrix(1.0);
  glm::mat4 quat_mat(mouse_button_info.quat);
  glm::vec3 ep_ori = get_eye_position(quat_mat);
  glm::vec3 ep = ep_ori;
  glm::vec3 rc(0,0,0);
  glm::vec3 up = get_camera_up_direction(quat_mat);
  view_matrix = glm::lookAt(ep, rc, up);
  return view_matrix;
}

glm::mat4 get_view_rotation_translation_matrix() {

  glm::mat4 view_matrix(1.0);
  glm::mat4 quat_mat(mouse_button_info.quat);
  glm::vec3 ep_ori = get_eye_position(quat_mat);
  glm::vec3 ep = ep_ori + display_info_t::rotation_centre;
  glm::vec3 rc = display_info_t::rotation_centre;
  glm::vec3 up = get_camera_up_direction(quat_mat);
  view_matrix = glm::lookAt(ep, rc, up);
  return view_matrix;

}

void
render_scene() {

   glEnable(GL_DEPTH_TEST);

   glm::mat4 mvp = get_mvp();
   glm::mat4 mvp_particle = get_particle_mvp();

   glm::mat4 quat_mat(mouse_button_info.quat);
   glm::vec3 ep = get_eye_position(quat_mat);

   display_info_t di;
   glm::vec4 bg_col(di.bg_col, 1.0f);

   if (false) {
      // debugging specular lighting:
      // what is the vector between the eye position and 100 times the light direction
      // after they have been multiplied by the view rotation matrix/quat_mat?
      glm::vec4 ep_post_rotation = glm::vec4(ep, 1.0) * quat_mat;
      glm::vec4 light_dir_post_rotation = glm::vec4(di.lights[0].direction, 1.0) * quat_mat;

      std::cout << "delta eye and light " << glm::to_string(light_dir_post_rotation - 100.0f * ep_post_rotation)
                << " " << glm::distance(light_dir_post_rotation, 100.0f * ep_post_rotation) << std::endl;
   }

   mesh_for_2_triangles.draw( &hello_shader,                     mvp, quat_mat, di.lights, ep, bg_col, di.do_depth_fog);

   if (true) {
      mesh_for_rama_balls.draw(&rama_balls_shader,           mvp, quat_mat, di.lights, ep, bg_col, di.do_depth_fog);
      mesh_for_instanced_balls.draw(&instanced_balls_shader, mvp, quat_mat, di.lights, ep, bg_col, di.do_depth_fog);
      mesh_for_cis_peptide.draw(&rama_balls_shader,          mvp, quat_mat, di.lights, ep, bg_col, di.do_depth_fog);
      mesh_for_octaspheres.draw(&octaspheres_shader,         mvp, quat_mat, di.lights, ep, bg_col, di.do_depth_fog);
      mesh_for_particles.draw_particles(&camera_facing_quad_shader, mvp_particle, quat_mat);
      // mesh_for_instanced_octahemispheres.draw_normals(mvp);
   }

   if (false) {
      std::vector<std::tuple<std::string, std::string, int> >
         texture_name_and_unit =
         {
          std::tuple<std::string, std::string, int> ("backpack-diffuse",   "base_texture",  0),
          std::tuple<std::string, std::string, int> ("backpack-normal",    "normal_map",    1),
          std::tuple<std::string, std::string, int> ("backpack-occlusion", "occlusion_map", 2),
          std::tuple<std::string, std::string, int> ("backpack-roughness", "roughness_map", 3),
          std::tuple<std::string, std::string, int> ("backpack-specular",  "specular_map",  4),
         };

      for (unsigned int ii=0; ii<texture_name_and_unit.size(); ii++) {
         std::string texture_name = std::get<0>(texture_name_and_unit[ii]);
         int unit = std::get<2>(texture_name_and_unit[ii]);
         std::map<std::string, Texture>::iterator it = display_info_t::textures.find(texture_name);
         if (it == display_info_t::textures.end()) {
            std::cout << "failed to find texture " << texture_name << std::endl;
         } else {
            std::string uniform_name = std::get<1>(texture_name_and_unit[ii]);
            it->second.Bind(unit);
         }
      }
   }

   di.draw_models(&obj_shader, &molecular_triangles_shader, mvp, quat_mat, ep);


   obj_shader.Use();
   di.draw_texture_meshes(&obj_shader, mvp, quat_mat, ep, bg_col);

   // position normal and colour
   di.draw_meshes(&molecular_triangles_shader, mvp, quat_mat, ep);

   // di.draw_meshes(&molecular_triangles_shader, mvp, quat_mat, ep, bg_col);

   // for (unsigned int ii=0; ii<di.models.size(); ii++)
   // di.models[ii].draw_tmeshes(&obj_shader, mvp, quat_mat, di.lights, ep, bg_col, true);

   // for (unsigned int ii=0; ii<di.models.size(); ii++)
   // di.models[ii].draw_meshes(&molecular_triangles_shader, mvp, quat_mat, di.lights, ep, bg_col, true);
   // a text box with "A test label" on it
   // mesh_for_camera_facing_quad.draw(&camera_facing_quad_shader, mvp, quat_mat, di.lights, ep, bg_col, di.do_depth_fog);

   // bonds: cylinders, octahemi bond caps
   bool draw_test_molecule = false;  // the pre-moleculartriangles tyrosine.
   if (draw_test_molecule) {
      mesh_for_instanced_cylinders.draw(&instanced_cylinder_shader, mvp, quat_mat, di.lights, ep, bg_col, di.do_depth_fog);
      mesh_for_instanced_octahemispheres.draw(&instanced_cylinder_shader, mvp, quat_mat, di.lights, ep, bg_col, di.do_depth_fog);
   }

   if (false) {
      glEnable(GL_BLEND);
      glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
      texture_for_camera_facing_quad.Bind(0);
      tmesh_for_camera_facing_quad.draw(&camera_facing_quad_shader, mvp, quat_mat, di.lights, ep,
                                        bg_col, di.do_depth_fog);
   }

   GLenum err = glGetError(); if (err) std::cout << "render_scene() D err " << err << std::endl;

   if (false) {
      glEnable(GL_BLEND);
      glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
      glm::vec3 offset_atom_position(0.2,0.3,0.2);
      glm::vec4 col(0.7, 0.2, 0.9, 1.0);
      bool is_perspective_projection = true;
      tmesh_for_atom_labels.draw_atom_label("//A/test/atom/label",
                                            offset_atom_position, col,
                                            &shader_for_atom_labels,
                                            mvp, quat_mat, di.lights,
                                            ep, bg_col, di.do_depth_fog, is_perspective_projection);
   }


   glm::vec4 col(0.7, 0.2, 0.9, 1.0);
   di.draw_atom_labels(tmesh_for_atom_labels, &shader_for_atom_labels, mvp, quat_mat, ep, bg_col);

   glDisable(GL_BLEND);

}

void
resize(GtkGLArea *gl_area, gint width, gint height) {

   gtk_gl_area_attach_buffers(GTK_GL_AREA(display_info_t::gl_area));

   GtkAllocation allocation;
   gtk_widget_get_allocation(GTK_WIDGET(gl_area), &allocation);
   // std::cout << "resize() debug:: " << allocation.width << " " << allocation.height << std::endl;
   width  = allocation.width;
   height = allocation.height;

   if (display_info_t::use_framebuffer_for_screen) {
      unsigned int index_offset = 0;
      if (false)
         std::cout << "calling init on framebuffer_for_screen "
                   << "scale " << display_info_t::framebuffer_scale << " " 
                   << " gl_area " << " " << gl_area
                   << " " << width << "x" << height << " " << std::endl;
      display_info_t::framebuffer_for_screen.init(display_info_t::framebuffer_scale * width,
                                                  display_info_t::framebuffer_scale * height,
                                                  index_offset, "screen/occlusion");
   }

}

void render_scene_to_base_framebuffer() {

   display_info_t di;
   glEnable(GL_DEPTH_TEST);
   shader_for_screen.Use();
   bool do_ambient_occlusion = di.do_ambient_occlusion;
   glBindVertexArray(screen_quad_vertex_array_id);

   glClearColor(0.0, 0.0, 0.0, 1.0);
   glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

   glActiveTexture(GL_TEXTURE0 + 0);
   glBindTexture(GL_TEXTURE_2D, di.framebuffer_for_screen.get_texture_colour());
   glActiveTexture(GL_TEXTURE0 + 1);
   glBindTexture(GL_TEXTURE_2D, di.framebuffer_for_screen.get_texture_depth());
   shader_for_screen.set_int_for_uniform("screenTexture", 0);
   shader_for_screen.set_int_for_uniform("screenDepth", 1);
   GLenum err = glGetError(); if (err) std::cout << "render() D err " << err << std::endl;
   shader_for_screen.set_bool_for_uniform("do_ambient_occlusion", do_ambient_occlusion);
   shader_for_screen.set_bool_for_uniform("do_outline", di.do_outline);

   glDrawArrays(GL_TRIANGLES, 0, 6);
   err = glGetError(); if (err) std::cout << "render() E err " << err << std::endl;
}


static gboolean
render (GtkGLArea    *gl_area,
        GdkGLContext *context) {

   display_info_t di;

   if (gtk_gl_area_get_error (gl_area) != NULL)
      return FALSE;

   if (di.use_framebuffer_for_screen) {
      GtkAllocation allocation;
      gtk_widget_get_allocation(di.gl_area, &allocation);
      int w = allocation.width;
      int h = allocation.height;

      glViewport(0, 0, di.framebuffer_scale * w, di.framebuffer_scale * h);
      di.framebuffer_for_screen.bind();

      glClearColor(di.bg_col[0], di.bg_col[1], di.bg_col[2], 1.0);
      glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

      render_scene();

      glViewport(0, 0, w, h);
      gtk_gl_area_attach_buffers(gl_area);

      render_scene_to_base_framebuffer();
   } else {
      gtk_gl_area_attach_buffers(gl_area);
      glClearColor(di.bg_col[0], di.bg_col[1], di.bg_col[2], 1.0);
      glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
      render_scene();
   }
   glFlush ();
   return TRUE;
}

void
test_python() {

   // py::exec("import hmt");
   // py::exec("print(hmt.add(1,2))");

   try {

      py::scoped_interpreter guard{}; // start the interpreter and keep it alive
      
      auto fast_calc = py::module::import("fast_calc");
      auto result = fast_calc.attr("add")(1, 2).cast<int>();
      assert(result == 3);

      py::exec("print(1 + 2)");
      
      py::module sys1 = py::module::import("sys");
      py::print(sys1.attr("path"));
      
      py::module sys2 = py::module::import("fast_calc");

      auto hmt = py::module::import("hmt");
      auto r_1 = hmt.attr("add")(1, 2).cast<int>();

      std::cout << "r_1 " << r_1 << std::endl;

      hmt.attr("black")().cast<void>();

      py::exec("print('Hello World')");

   }
   catch (pybind11::error_already_set &err) {
      std::cout << "error: " << err.what() << std::endl;
   }

}

// --------------- these background functions are not wired int the gui buttons --- 

void
black_background() {
   std::cout << "black background" << std::endl;
   display_info_t::bg_col = glm::vec3(0.0, 0.0, 0.0);
   gtk_widget_queue_draw(display_info_t::gl_area);
}

void
white_background() {
   std::cout << "white background" << std::endl;
   display_info_t::bg_col = glm::vec3(1.0, 1.0, 1.0);
   gtk_widget_queue_draw(display_info_t::gl_area);
}

void
grey_background() {
   std::cout << "grey background" << std::endl;
   float gl = 0.99;
   display_info_t::bg_col = glm::vec3(gl, gl, gl);
   gtk_widget_queue_draw(display_info_t::gl_area);
}

void set_draw_ao(bool state) {
   display_info_t::do_ambient_occlusion = state;
   gtk_widget_queue_draw(display_info_t::gl_area);
}

void set_draw_fog(bool state) {
   display_info_t::do_depth_fog = state;
   gtk_widget_queue_draw(display_info_t::gl_area);
}

void quick_ribs() {
   display_info_t di;
   di.attach_buffers();

   molecular_mesh_generator_t mmg;
   std::vector<molecular_triangles_mesh_t> mtm =
      mmg.get_molecular_triangles_mesh(di.mol, "//", "Chain", "Ribbon");

   Material material;
   material.shininess = 700;
   material.specular_strength = 0.5;
   di.add_model(mtm, &molecular_triangles_shader, material);

   // does this work with Models?
   add_this_rep_to_representations_table();
}

void quick_surf() {
   display_info_t di;
   di.attach_buffers();
   Material material;

   // std::string name = "MolecularSurface";
   // Mesh mesh(name);
   // molecular_mesh_generator_t mmg;
   // di.add_mesh(mesh);
   // di.meshes.back().import(mmg.get_molecular_triangles_mesh(display_info_t::mol, "//", "Chain", "MolecularSurface"));
   // di.meshes.back().setup(&molecular_triangles_shader, material);

   molecular_mesh_generator_t mmg;
   std::vector<molecular_triangles_mesh_t> mtm =
      mmg.get_molecular_triangles_mesh(di.mol, "//", "Chain", "MolecularSurface");
   di.add_model(mtm, &molecular_triangles_shader, material);

   add_this_rep_to_representations_table();
}

void quick_ligands() {

   if (! display_info_t::mol) return;

   int imod = 1;
   mmdb::Model *model_p = display_info_t::mol->GetModel(imod);
   if (model_p) {
      display_info_t::attach_buffers();
      std::string name = "Ligands";
      int n_chains = model_p->GetNumberOfChains();
      for (int ichain=0; ichain<n_chains; ichain++) {
         mmdb::Chain *chain_p = model_p->GetChain(ichain);
         int nres = chain_p->GetNumberOfResidues();
         for (int ires=0; ires<nres; ires++) {
            mmdb::Residue *residue_p = chain_p->GetResidue(ires);
            if (!residue_p) continue;
            std::string rn(residue_p->GetResName());
            if (rn == "HOH") continue;
            int n_atoms = residue_p->GetNumberOfAtoms();
            if (n_atoms > 0) {
               mmdb::Atom *at = residue_p->GetAtom(0);
               if (at->Het) {
                  std::string col_selection = "/*/*/*/*[C].*";
                  std::string col_colour = "#dddd55";
                  int res_no = residue_p->GetSeqNum();
                  std::string selection_string = "//" + std::string(chain_p->GetChainID())
                     + "/" + std::to_string(res_no);

                  // instead of adding the model here, the mesh should be
                  // accumulated and added at the end.

                  Material material;
                  display_info_t di;
                  molecular_mesh_generator_t mmg;
                  mmg.add_selection_and_colour(col_selection, col_colour);
                  std::vector<molecular_triangles_mesh_t> mtm =
                     mmg.get_molecular_triangles_mesh(di.mol, selection_string,
                                                      "Element", "Cylinders");
                  di.add_model(mtm, &molecular_triangles_shader, material);
               }
            }
         }
      }
      add_this_rep_to_representations_table();
   }
}

std::vector<mmdb::Residue *>
get_hetgroups(mmdb::Manager *mol) {

   std::vector<mmdb::Residue *> v;
   if (!mol) return v;

   int imod = 1;
   mmdb::Model *model_p = display_info_t::mol->GetModel(imod);
   if (model_p) {
      display_info_t::attach_buffers();
      Material material;
      std::string name = "Ligands";
      Mesh mesh(name);
      molecular_mesh_generator_t mmg;
      int n_chains = model_p->GetNumberOfChains();
      for (int ichain=0; ichain<n_chains; ichain++) {
         mmdb::Chain *chain_p = model_p->GetChain(ichain);
         int nres = chain_p->GetNumberOfResidues();
         for (int ires=0; ires<nres; ires++) {
            mmdb::Residue *residue_p = chain_p->GetResidue(ires);
            if (!residue_p) continue;
            std::string rn(residue_p->GetResName());
            if (rn == "HOH") continue;
            int n_atoms = residue_p->GetNumberOfAtoms();
            if (n_atoms > 0) {
               mmdb::Atom *at = residue_p->GetAtom(0);
               if (at->Het) {
                  v.push_back(residue_p);
               }
            }
         }
      }
   }
   return v;
}


#include "coot-coord-utils.hh"
#include <clipper/core/coords.h>

// does residue_p interact with the ligand via a main-chain atom:
bool has_main_chain_contact(mmdb::Residue *residue_p, mmdb::Residue *ligand_p) {

   bool state = false;
   const float dist_max = 3.6;
   mmdb::Atom **residue_atoms = 0;
   int n_residue_atoms = 0;
   residue_p->GetAtomTable(residue_atoms, n_residue_atoms);
   for (unsigned int iat=0; iat<n_residue_atoms; iat++) {
      mmdb::Atom *at = residue_atoms[iat];
      if (! at->isTer()) {
         std::string atom_name(at->GetAtomName());
         clipper::Coord_orth p1(at->x, at->y, at->z);
         mmdb::Atom **ligand_residue_atoms = 0;
         int n_ligand_residue_atoms = 0;
         ligand_p->GetAtomTable(ligand_residue_atoms, n_ligand_residue_atoms);
         for(int jat=0; jat<n_ligand_residue_atoms; jat++) {
            mmdb::Atom *at_l = ligand_residue_atoms[jat];
            if (! at_l->isTer()) {
               if (atom_name == " C  " || atom_name == " N  " || atom_name == " O  ") {
                  clipper::Coord_orth p2(at_l->x, at_l->y, at_l->z);
                  if ((p1-p2).lengthsq() < dist_max * dist_max) {
                     state = true;
                     break;
                  }
               }
            }
         }
         if (state) break;
      }
   }
   return state;
}

void quick_ligand_neighbours() {

   display_info_t di;

   std::vector<mmdb::Residue *> het_groups = get_hetgroups(di.mol);
   if (het_groups.empty()) return;

   mmdb::Residue *ligand_residue = di.choose_closest_ligand(het_groups);
   float radius = 4.0;

   std::vector<mmdb::Residue *> neighbs = coot::residues_near_residue(ligand_residue, di.mol, radius);

   // It would be nice if neighbs was a vector of a vector, where sequence sequential residues
   // are in the same vector - then we could combine atom selections and draw the mainchain
   // peptide bond.

   if (! neighbs.empty()) {
      di.attach_buffers();
      Material material;
      std::string name = "Ligand Neighbours";
      Mesh mesh(name);
      molecular_mesh_generator_t mmg;
      for (unsigned int i=0; i<neighbs.size(); i++) {
         int rn = neighbs[i]->GetSeqNum();
         bool mcc = has_main_chain_contact(neighbs[i], ligand_residue);
         std::string main_chain_selection = "/!O,C,N";
         if (mcc)
            main_chain_selection = "";
         std::string col_selection = "/*/*/*/*[C].*";
         std::string col_colour = "#bb9944";
         mmg.add_selection_and_colour(col_selection, col_colour);
         std::string chain_id = std::string(neighbs[i]->chain->GetChainID());
         std::string selection_string = "//" + chain_id + "/" + std::to_string(rn) + main_chain_selection;
         // This needs more thought - another time
         // mesh.import(mmg.get_molecular_triangles_mesh(display_info_t::mol, selection_string,
         // "Element", "Cylinders"));
      }
      di.add_mesh(mesh);
      di.meshes.back().setup(&molecular_triangles_shader, material);
      add_this_rep_to_representations_table();
   }

}

void
quick_waters() {
   display_info_t di;
   di.attach_buffers();

#if 0  // more thought
   Mesh mesh(name);
   Material material;
   std::string name = "Waters";
   molecular_mesh_generator_t mmg;
   di.add_mesh(mesh);
   di.meshes.back().import(mmg.get_molecular_triangles_mesh(di.mol, "//*/(HOH)", "Element", "Cylinders"));
   di.meshes.back().setup(&molecular_triangles_shader, material);
   add_this_rep_to_representations_table();
#endif
}



std::vector<mmdb::Chain *>
chains_in_mol(mmdb::Manager *mol) {

   std::vector<mmdb::Chain *> v;
   if (mol) {
      int imod = 1;
      mmdb::Model *model_p = mol->GetModel(imod);
      if (model_p) {
         int n_chains = model_p->GetNumberOfChains();
         for (int ichain=0; ichain<n_chains; ichain++) {
            v.push_back(model_p->GetChain(ichain));
         }
      }
   }
   return v;
}

#include "residue-and-atom-specs.hh"
#include "cylinder.hh"

glm::vec3
mmdb_atom_to_glm(mmdb::Atom *at) {
   return glm::vec3(at->x, at->y, at->z);
}

void
add_atom_label(const coot::atom_spec_t &spec,
               const glm::vec4 &col,
               mmdb::Manager *mol) {

   if (mol) {
      mmdb::Atom *label_atom = 0;
      int imod = 1;
      mmdb::Model *model_p = mol->GetModel(imod);
      if (model_p) {
         int n_chains = model_p->GetNumberOfChains();
         for (int ichain=0; ichain<n_chains; ichain++) {
            mmdb::Chain *chain_p = model_p->GetChain(ichain);
            std::string chain_id = chain_p->GetChainID();
            int nres = chain_p->GetNumberOfResidues();
            for (int ires=0; ires<nres; ires++) {
               mmdb::Residue *residue_p = chain_p->GetResidue(ires);
               int n_atoms = residue_p->GetNumberOfAtoms();
               for (int iat=0; iat<n_atoms; iat++) {
                  mmdb::Atom *at = residue_p->GetAtom(iat);
                  if (! at->isTer()) {
                     coot::atom_spec_t this_atom_spec(at);
                     if (this_atom_spec == spec) {
                        label_atom = at;
                        break;
                     }
                  }
               }
            }
            if (label_atom)
               break;
         }

         if (label_atom) {
            display_info_t di;
            coot::atom_spec_t la(label_atom);
            std::string rn = label_atom->residue->GetResName();
            std::string text = la.label(rn);
            glm::vec3 pos = mmdb_atom_to_glm(label_atom);
            glm::vec3 offset_position = pos + glm::vec3(0.2, 0.2, 0.2);
            di.add_atom_label(text, offset_position, col);
         }
      }
   }
}

void
add_H_bond(const coot::atom_spec_t &spec_1,
           const coot::atom_spec_t &spec_2,
           mmdb::Manager *mol) {

   if (mol) {

      mmdb::Atom *at_1 = 0;
      mmdb::Atom *at_2 = 0;

      int imod = 1;
      mmdb::Model *model_p = mol->GetModel(imod);
      if (model_p) {
         int n_chains = model_p->GetNumberOfChains();
         for (int ichain=0; ichain<n_chains; ichain++) {
            mmdb::Chain *chain_p = model_p->GetChain(ichain);
            std::string chain_id = chain_p->GetChainID();
            int nres = chain_p->GetNumberOfResidues();
            for (int ires=0; ires<nres; ires++) {
               mmdb::Residue *residue_p = chain_p->GetResidue(ires);
               int n_atoms = residue_p->GetNumberOfAtoms();
               for (int iat=0; iat<n_atoms; iat++) {
                  mmdb::Atom *at = residue_p->GetAtom(iat);
                  if (! at->isTer()) {
                     coot::atom_spec_t this_atom_spec(at);
                     if (spec_1.chain_id == chain_id) {
                        if (residue_p->GetSeqNum() == spec_1.res_no) {
                           if (std::string(at->GetAtomName()) == spec_1.atom_name) {
                              at_1 = at;
                           }
                        }
                     }
                     if (spec_2.chain_id == chain_id) {
                        if (residue_p->GetSeqNum() == spec_2.res_no) {
                           if (std::string(at->GetAtomName()) == spec_2.atom_name) {
                              at_2 = at;
                           }
                        }
                     }
                  }
               }
            }
         }
      }

      std::cout << "add_H_bond a " << at_1 << " " << at_2 << std::endl;

      if (at_1 && at_2) {
         std::cout << "Found the atoms" << std::endl;
         glm::vec3 pos_1(at_1->x, at_1->y, at_1->z);
         glm::vec3 pos_2(at_2->x, at_2->y, at_2->z);

         float d = glm::distance(pos_1, pos_2);
         glm::vec3 n = glm::normalize(pos_2-pos_1);

         std::string mesh_name = "H-bond ";
         mesh_name += spec_1.chain_id + " " + std::to_string(spec_1.res_no) + " " + spec_1.atom_name;
         mesh_name += spec_2.chain_id + " " + std::to_string(spec_2.res_no) + " " + spec_2.atom_name;
         Mesh mesh(mesh_name);

         glm::vec4 col(0.7, 0.2, 0.95, 1.0);
         for (unsigned int i=0; i<20; i+=2) {
            glm::vec3 pt_1 = pos_1 + 0.05f * d * n * static_cast<float>(i);
            glm::vec3 pt_2 = pos_1 + 0.05f * d * n * static_cast<float>(i+1);
            std::pair<glm::vec3, glm::vec3> pp(pt_1, pt_2);
            cylinder c(pp, 0.07, 0.07, d * 0.05);
            c.add_flat_start_cap();
            c.add_flat_end_cap();
            for (unsigned int j=0; j<c.vertices.size(); j++)
               c.vertices[j].color = col;
            mesh.import(c.vertices, c.triangle_indices_vec);
         }

         Material material;
         mesh.setup(&molecular_triangles_shader, material);
         display_info_t di;
         di.add_mesh(mesh);

         // label
         glm::vec3 mid_point = 0.5f * (pos_1 + pos_2);
         glm::vec3 offset_mid_point = mid_point + glm::vec3(0.15, 0.05, 0.05);
         std::ostringstream ss;
         ss << std::setprecision(3) << std::fixed << d;
         std::string text(ss.str());
         di.add_atom_label(text, offset_mid_point, 0.8f * col);
      }
   }

   // B 54 N4 to B 72 O6

}


void
on_entry_activate(GtkEntry *entry, gpointer  user_data) {

   std::string t = gtk_entry_get_text(entry);

   try {
      parse_text(t);
   }
   catch(const py::error_already_set &e) {
      std::cout << "error " << e.what() << std::endl;
   }

   gtk_entry_set_text(entry, "");
}

void
setup_gtk_gl_area_widget(GtkWidget *glarea_box, GtkWidget *box_for_reps) {

   display_info_t di;
   di.rep_box = box_for_reps;
   di.gl_area = gtk_gl_area_new(); // gl_area is global
   gtk_widget_set_hexpand (di.gl_area, TRUE);
   gtk_widget_set_vexpand (di.gl_area, TRUE);
   gtk_box_pack_start(GTK_BOX(glarea_box), di.gl_area, TRUE, TRUE, 0);

   gtk_box_reorder_child(GTK_BOX(glarea_box), di.gl_area, 0);

   gtk_widget_set_size_request(di.gl_area, 500, 500);

   /* We need to initialize and free GL resources, so we use
    * the realize and unrealize signals on the widget
    */
   g_signal_connect (di.gl_area, "realize",   G_CALLBACK(realize),   NULL);
   g_signal_connect (di.gl_area, "unrealize", G_CALLBACK(unrealize), NULL);
   /* The main "draw" call for GtkGLArea */
   g_signal_connect (di.gl_area, "render", G_CALLBACK (render), NULL);
   g_signal_connect (di.gl_area, "resize",  G_CALLBACK(resize),  NULL);

   my_glarea_add_other_signals_and_events(di.gl_area); // mouse and keyboard

}


gboolean
on_glarea_scroll(GtkWidget *widget, GdkEventScroll *event) {

   double delta = 14.0;
   if (event->direction == GDK_SCROLL_UP)
      delta = -delta;
   float sf = 1.0 - delta * 0.0028f;
   distance_of_eye_to_rotation_centre *= sf;
   gtk_widget_queue_draw(widget);
   return FALSE;
}

gboolean
on_glarea_button_press(GtkWidget *widget, GdkEventButton *event) {
   mouse_button_info.add_press(event->x, event->y);

   return TRUE;
}

void
atom_pick(int x, int y) {

   display_info_t di;
   GtkAllocation allocation;
   gtk_widget_get_allocation(di.gl_area, &allocation);
   int w = allocation.width;
   int h = allocation.height;
   // event->x, event->y;

   float screen_ratio = static_cast<float>(w)/static_cast<float>(h);
   float mouseX = x / (w * 0.5f) - 1.0f;
   float mouseY = y / (h * 0.5f) - 1.0f;
   glm::mat4 mvp = get_mvp();
   glm::mat4 vp_inv = glm::inverse(mvp);
   float real_y = - mouseY; // in range -1 -> 1
   glm::vec4 screenPos_f = glm::vec4(mouseX, real_y, -1.0f, 1.0f);
   glm::vec4 screenPos_b = glm::vec4(mouseX, real_y,  1.0f, 1.0f); // or other way round?
   glm::vec4 worldPos_f = vp_inv * screenPos_f;
   glm::vec4 worldPos_b = vp_inv * screenPos_b;
   float w_scale_f = 1.0/worldPos_f.w;
   float w_scale_b = 1.0/worldPos_b.w;
   glm::vec3 front(worldPos_f.x * w_scale_f, worldPos_f.y * w_scale_f, worldPos_f.z * w_scale_f);
   glm::vec3  back(worldPos_b.x * w_scale_b, worldPos_b.y * w_scale_b, worldPos_b.z * w_scale_b);
   glm::vec3 back_to_front = front - back;

   float dist_closest = 999999999999999999.9;

   auto l = [front, back, back_to_front](const glm::vec3 &pos) {
               glm::vec3 v1(pos-front);
               glm::vec3 v2(pos-back);
               float dp_1 = glm::dot(back_to_front, -v1);
               float dp_2 = glm::dot(back_to_front, -v2);
               if (dp_1 > 0.0f) {
                  if (dp_2 > 0.0f) {
                     
                  }
               }
            };

   if (display_info_t::mol) {
      int imod = 1;
      mmdb::Model *model_p = display_info_t::mol->GetModel(imod);
      if (model_p) {
         int n_chains = model_p->GetNumberOfChains();
         for (int ichain=0; ichain<n_chains; ichain++) {
            mmdb::Chain *chain_p = model_p->GetChain(ichain);
            int nres = chain_p->GetNumberOfResidues();
            for (int ires=0; ires<nres; ires++) {
               mmdb::Residue *residue_p = chain_p->GetResidue(ires);
               int n_atoms = residue_p->GetNumberOfAtoms();
               for (int iat=0; iat<n_atoms; iat++) {
                  mmdb::Atom *at = residue_p->GetAtom(iat);
                  if (! at->isTer()) {
                     glm::vec3 at_pos(at->x, at->y, at->z);
                     // float dd = distance_sqd_to_line(at_pos);
                  }
               }
            }
         }
      }
   }
   

}

gboolean
on_glarea_button_release(GtkWidget *widget, GdkEventButton *event) {

   if (event->state & GDK_BUTTON2_MASK) {
      atom_pick(event->x, event->y);
   }

   mouse_button_info.clear_press();
   return TRUE;
}

float
trackball_project_to_sphere(float r, float x, float y) {

    float oor2 = 0.70710678118654752440;
    float d = sqrt(x*x + y*y);
    if (d < r * oor2) {    /* Inside sphere */
        return sqrt(r*r - d*d);
    } else {
       /* On hyperbola */
       float t = r * oor2;
       return t*t/d;
    }
}

glm::quat
trackball_to_quaternion(float p1x, float p1y, float p2x, float p2y, float trackball_size) {

   if (p1x == p2x && p1y == p2y) {
        /* Zero rotation */
        return glm::quat(1,0,0,0);
    }

    float d_mult = trackball_size - 0.3; /* or some such */

    /*
     * First, figure out z-coordinates for projection of P1 and P2 to
     * deformed sphere
     */
    glm::vec3 p1(p1x, p1y, trackball_project_to_sphere(trackball_size, p1x, p1y));
    glm::vec3 p2(p2x, p2y, trackball_project_to_sphere(trackball_size, p2x, p2y));

    /*
     *  Now, we want the cross product of P1 and P2
     */
    // vcross(p2,p1,a);
    glm::vec3 a = glm::normalize(glm::cross(p2,p1));

    /*
     *  Figure out how much to rotate around that axis.
     */
    glm::vec3 d(p1 - p2);

    float t = glm::length(d) * d_mult / (trackball_size);

    /*
     * Avoid problems with out-of-control values...
     */
    if (t >  1.0f) t =  1.0f;
    if (t < -1.0f) t = -1.0f;

    /* how much to rotate about axis */
    float phi = 2.0f * asin(t);

    glm::vec3 qaaa(a * sinf(phi/2.0f));
    glm::quat q(cosf(phi/2.0f), qaaa.x, qaaa.y, qaaa.z);
    return q;
}

void
display_info_t::add_to_rotation_centre(const glm::vec3 &delta) {
   rotation_centre += delta;
}

void
do_drag_pan(GtkWidget *widget, GdkEventMotion *event) {

   GtkAllocation allocation;
   gtk_widget_get_allocation(widget, &allocation);
   int w = allocation.width;
   int h = allocation.height;
   glm::mat4 mvp = get_mvp();

   float z_position = -1.0;

   float mouseX_1 = mouse_button_info.previous_at_x / (w * 0.5f) - 1.0f;
   float mouseY_1 = mouse_button_info.previous_at_y / (h * 0.5f) - 1.0f;
   float mouseX_2 = event->x  / (w * 0.5f) - 1.0f;
   float mouseY_2 = event->y  / (h * 0.5f) - 1.0f;

   glm::mat4 vp_inv = glm::inverse(mvp);

   glm::vec4 screenPos_1 = glm::vec4(mouseX_1, -mouseY_1, 1.0f, z_position);
   glm::vec4 screenPos_2 = glm::vec4(mouseX_2, -mouseY_2, 1.0f, z_position);
   glm::vec4 worldPos_1 = vp_inv * screenPos_1;
   glm::vec4 worldPos_2 = vp_inv * screenPos_2;

   glm::vec4 delta(worldPos_1 / worldPos_1.w - worldPos_2 / worldPos_2.w);
   glm::vec3 delta_v3(delta);

   // If you have done the transformations correctly you don't need this
   // scale factor.
   // This is still hacky, but the translation is better than it was.

   // I changed the distance to the front clipping plane so that it is quite far
   // back (give makes the ambient occlusion filter have some effect), but now
   // we need to scale down the translation a lot
   // z_front =  0.1, needs -8.0
   // z_front = 65.1, needs -0.014
   // scale = 1.0/z_front perhaps?
   float scale = 1.0/calc_z_front_real();
   display_info_t di;
   di.add_to_rotation_centre(-scale * distance_of_eye_to_rotation_centre * delta_v3);
}

void
do_zoom(GtkWidget *widget, GdkEventMotion *event) {
   double delta_x = event->x - mouse_button_info.previous_at_x;
   float sf = 1.0 - delta_x * 0.0038f;
   distance_of_eye_to_rotation_centre *= sf;
   // std::cout << "distance_of_eye_to_rotation_centre " << distance_of_eye_to_rotation_centre << std::endl;
}

void
update_view_rotation(GtkWidget *widget, GdkEventMotion *event) {

   GtkAllocation allocation;
   gtk_widget_get_allocation(widget, &allocation);
   int w = allocation.width;
   int h = allocation.height;
   float tbs = 10.0;

   glm::quat tb_quat_delta =
      trackball_to_quaternion((2.0*mouse_button_info.previous_at_x - w)/w,
                              (h - 2.0*mouse_button_info.previous_at_y)/h,
                              (2.0*event->x - w)/w,
                              (h - 2.0*event->y)/h,
                              tbs);

   glm::quat tb_delta_conj = glm::conjugate(tb_quat_delta);
   mouse_button_info.add_rotation_delta(tb_delta_conj);

}

gboolean
on_glarea_motion_notify(GtkWidget *widget, GdkEventMotion *event) {

   if (mouse_button_info.is_pressed) {

      if (false)
         std::cout << "delta from previous: "
                   << event->x - mouse_button_info.previous_at_x << " "
                   << event->y - mouse_button_info.previous_at_y << std::endl;

      if (event->state & GDK_BUTTON1_MASK) {
         update_view_rotation(widget, event);
      }

      if (event->state & GDK_BUTTON2_MASK) {
         do_drag_pan(widget, event);
      }

      if (event->state & GDK_BUTTON3_MASK) {
         do_zoom(widget, event);
      }

      // for next time
      mouse_button_info.update_previous(event->x, event->y);
      gtk_widget_queue_draw(widget);
   }
   return TRUE;
}

bool do_tick_model_update = true;
bool do_tick_spin = true;  // view spin
bool do_tick_particles = true;
bool do_tick_light_spin = false;
float light_spin_theta = 0.0f;  // radians
unsigned int light_spin_count = 0;

gboolean
glarea_tick_func(GtkWidget *widget,
                 GdkFrameClock *frame_clock,
                 gpointer data) {


   gboolean r = TRUE; // tick again by default

   if (widget) {

      if (do_tick_model_update) {

         mol_mesh_gen.move_the_atoms_and_update_the_instancing_matrices();
         std::cout << "tick: do_tick_model_update " << mol_mesh_gen.bond_instance_matrices.size() << std::endl;
         mesh_for_instanced_cylinders.update_instancing_buffer_data(mol_mesh_gen.bond_instance_matrices,
                                                                    mol_mesh_gen.bond_instance_colours);
         mesh_for_instanced_octahemispheres.update_instancing_buffer_data(mol_mesh_gen.atom_instance_matrices,
                                                                          mol_mesh_gen.atom_instance_colours);
      }

      if (do_tick_particles) {
         mesh_for_octaspheres.clear();
         particles.update_particles();
         mesh_for_particles.update_instancing_buffer_data_for_particles(particles);
      }

      if (do_tick_spin) {
         float delta = 0.005;

         glm::vec3 EulerAngles(0, delta, 0);
         glm::quat quat_delta(EulerAngles);
         mouse_button_info.add_rotation_delta(quat_delta);
      }

      if (do_tick_light_spin) {
         display_info_t di;
         float angle_frac = 6.283f / static_cast<float>(200);
         di.spin_the_lights(angle_frac);
         light_spin_count++;

         if (light_spin_count >= 1000) {
            light_spin_count = 0;
            do_tick_spin = false;
            r = FALSE;
         }
      }

      gtk_widget_queue_draw(widget);

      // stop ticking if we were doing particles and nothing else and run out of particles.
      if (! do_tick_spin)
         if (! do_tick_model_update)
            if (! do_tick_light_spin)
               if (particles.size() == 0) {
                  spin_tick_id = -1;
                  return FALSE;
               }


      return r;

   } else {
      return FALSE; // stop being called
   }
}

void
spin_the_lights() {

   do_tick_light_spin = true;
   do_tick_spin = false;
   spin_tick_id = gtk_widget_add_tick_callback(display_info_t::gl_area, glarea_tick_func, 0, 0);

}

#include "pumpkin.hh"

void
import_a_pumpkin() {

   display_info_t di;
   di.attach_buffers();

   // maybe try with a model later.
   std::pair<std::vector<position_normal_vertex>, std::vector<g_triangle> >  bits = pumpkin();
   std::pair<std::vector<position_normal_vertex>, std::vector<g_triangle> > stalk = pumpkin_stalk();

   glm::vec4 pumpkin_colour(0.9, 0.75, 0.3, 1.0);
   glm::vec4 stalk_colour(0.3, 0.85, 0.3, 1.0);
   Mesh m;
   m.import( bits.first,  bits.second, pumpkin_colour);
   m.import(stalk.first, stalk.second,   stalk_colour);

   Material material;
   material.specular_strength = 0.5;
   material.shininess = 12;
   Shader &shader = molecular_triangles_shader;
   m.setup(&shader, material);
   m.translate_by(di.rotation_centre);

   di.add_mesh(m);
}

gboolean
on_glarea_key_press_notify(GtkWidget *gl_area, GdkEventKey *event) {

   std::cout << "key press notify " << event << std::endl;

   if (event->keyval == GDK_KEY_i) {
      if (spin_tick_id  == -1) {
         // spin_timeout_id = g_timeout_add(16, view_spin_func, GINT_TO_POINTER(66));

         if (particles.size() == 0) {
            std::vector<glm::vec3> particle_positions;
            particle_positions.push_back(display_info_t::rotation_centre);
            particles.make_particles(n_particles, particle_positions);
         }

         spin_tick_id = gtk_widget_add_tick_callback(gl_area, glarea_tick_func, 0, 0);
      } else {
         gtk_widget_remove_tick_callback(gl_area, spin_tick_id);
         spin_tick_id = -1;
      }
   }
   if (event->keyval == GDK_KEY_n) {
      float sf = 1.0 - 0.02;
      distance_of_eye_to_rotation_centre *= sf;
      gtk_widget_queue_draw(gl_area);
   }
   if (event->keyval == GDK_KEY_m) {
      float sf = 1.0 + 0.02;
      distance_of_eye_to_rotation_centre *= sf;
      gtk_widget_queue_draw(gl_area);
   }
   if (event->keyval == GDK_KEY_s) {
      display_info_t di;
      int w = di.get_width();
      int h = di.get_height();
      int scale_factor = di.framebuffer_scale;
      screendump_tga("test.tga", h, w, scale_factor, di.framebuffer_for_screen);
   }
   return FALSE;
}

gboolean
on_glarea_key_release_notify(GtkWidget *widget, GdkEventKey *event) {
   return FALSE;
}




void
my_glarea_add_other_signals_and_events(GtkWidget *glarea) {

   gtk_widget_add_events(glarea, GDK_SCROLL_MASK);
   gtk_widget_add_events(glarea, GDK_BUTTON_PRESS_MASK);
   gtk_widget_add_events(glarea, GDK_BUTTON_RELEASE_MASK);
   gtk_widget_add_events(glarea, GDK_BUTTON1_MOTION_MASK);
   gtk_widget_add_events(glarea, GDK_BUTTON2_MOTION_MASK);
   gtk_widget_add_events(glarea, GDK_BUTTON3_MOTION_MASK);
   gtk_widget_add_events(glarea, GDK_POINTER_MOTION_MASK);
   gtk_widget_add_events(glarea, GDK_BUTTON1_MASK);
   gtk_widget_add_events(glarea, GDK_BUTTON2_MASK);
   gtk_widget_add_events(glarea, GDK_BUTTON3_MASK);
   gtk_widget_add_events(glarea, GDK_KEY_PRESS_MASK);

   // key presses for the glarea:

   gtk_widget_set_can_focus(glarea, TRUE);
   gtk_widget_grab_focus(glarea);

   g_signal_connect(glarea, "scroll-event",          G_CALLBACK(on_glarea_scroll),             NULL);
   g_signal_connect(glarea, "button-press-event",    G_CALLBACK(on_glarea_button_press),       NULL);
   g_signal_connect(glarea, "button-release-event",  G_CALLBACK(on_glarea_button_release),     NULL);
   g_signal_connect(glarea, "motion-notify-event",   G_CALLBACK(on_glarea_motion_notify),      NULL);
   g_signal_connect(glarea, "key-press-event",       G_CALLBACK(on_glarea_key_press_notify),   NULL);
   g_signal_connect(glarea, "key-release-event",     G_CALLBACK(on_glarea_key_release_notify), NULL);
}
