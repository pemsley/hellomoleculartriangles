
class mouse_button_info_t {
public:
   mouse_button_info_t() {
      is_pressed = false;
      glm::mat4 u(1.0);
      quat = glm::quat(u);
   }
   bool is_pressed;
   float pressed_at_x;
   float pressed_at_y;
   float previous_at_x;
   float previous_at_y;
   glm::quat quat;
   void add_press(float x, float y) {
      is_pressed = true;
      pressed_at_x = x;
      pressed_at_y = y;
      update_previous(x,y);
   }
   void update_previous(float x, float y) {
      previous_at_x = x;
      previous_at_y = y;
   }
   void clear_press() { // released
      is_pressed = false;
   }
   void add_rotation_delta(const glm::quat &quat_delta) {
      quat = quat_delta * quat;
   }
};

