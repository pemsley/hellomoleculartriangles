
#include <glm/ext.hpp>
#include <CXXSurface/CXXCoord.h>

glm::vec3 fcxxcoord_to_glm(FCXXCoord &c);
FCXXCoord glm_to_fcxxcoord(glm::vec3 &c);
