
// use the #shader "directive" to separate the shaders on parsing
// -----------------------------------------
#shader vertex

#version 330 core

layout(location = 0) in vec3 position;
layout(location = 1) in vec3 normal;
layout(location = 2) in vec4 colour;

uniform mat4 mvp;

out vec4 colour_transfer;
out vec3 normal_transfer;
out vec4 frag_pos_transfer;

void main() {
   vec4 n = vec4(normal, 1.0);
   vec4 frag_pos = vec4(position, 1.0);
   gl_Position = mvp * frag_pos;

   normal_transfer = normalize(n.xyz);
   colour_transfer = colour;
   frag_pos_transfer = frag_pos;
}


// -----------------------------------------
#shader fragment

#version 330 core

struct Material {
    vec4 emission;
    vec4 ambient;
    vec4 diffuse;
    vec4 specular;
    float shininess;
};
uniform Material material;

struct LightSource {
   bool is_on;
   bool directional;
   vec4 position;
   vec4 position_in_molecule_coordinates_space;
   vec4 ambient;
   vec4 diffuse;
   vec4 specular;
   vec4 halfVector;
   vec3 spotDirection;
   float spotExponent;
   float spotCutoff;
   float spotCosCutoff;
   float constantAttenuation;
   float linearAttenuation;
   float quadraticAttenuation;
};

uniform LightSource light_sources[2];
uniform vec3 eye_position;

in vec4 colour_transfer;
in vec4 normal_transfer;
in vec4 frag_pos_transfer;

out vec4 outputColor;

void main() {

   outputColor = vec4(0,0,0,0);

   for (int i=0; i<2; i++) {
      if (light_sources[i].is_on) {
         vec4 light_dir = light_sources[i].position_in_molecule_coordinates_space;
         float dp = dot(normal_transfer, light_dir);
         dp = max(dp, 0.05); // no negative dot products for diffuse for now, also, zero is avoided.

         vec4 ambient  = material.ambient * light_sources[i].ambient * 0.1;
         vec4 diffuse  = material.diffuse * light_sources[i].diffuse * dp * 0.8;

         // specular 
         vec3 eye_pos = eye_position;
         vec3 norm_2 = normalize(normal_transfer.xyz);
         vec3 view_dir = normalize(eye_pos - frag_pos_transfer.xyz);
         vec3 light_dir_v3 = light_dir.xyz;
         vec3 reflect_dir = reflect(light_dir_v3, norm_2);
         reflect_dir = normalize(reflect_dir); // belt and braces
         float dp_view_reflect = dot(view_dir, -reflect_dir);
         dp_view_reflect = max(dp_view_reflect, 0.0);
         dp_view_reflect = min(dp_view_reflect, 1.0);
         float spec = pow(dp_view_reflect, material.shininess);
         vec4 specular = spec * material.specular * light_sources[i].specular;
         
         outputColor += ambient + diffuse + specular;
      }
   }


}
