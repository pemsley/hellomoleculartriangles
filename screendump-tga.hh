
#ifndef SCREENDUMP_TGA_HH
#define SCREENDUMP_TGA_HH

#include "framebuffer.hh"

void screendump_tga(std::string tga_file,
                    int widget_height, int widget_width, int image_scale_factor,
                    framebuffer &framebuffer_for_screen);

#endif // SCREENDUMP_TGA_HH
