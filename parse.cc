
#include <string>
#include <vector>
#include <sstream>

#include "Model.hh"

#include "hmt.hh"
#include "display-info.hh"
#include "molecular-mesh-generator.hh"

extern Shader molecular_triangles_shader;

std::vector<std::string> split_command(const std::string &string_in) {

   std::vector<std::string> v;
   std::string s=string_in;

   while (true) {
      std::string splitter(" ");
      std::string::size_type isplit=s.find_first_of(splitter);
      if (isplit != std::string::npos) {
         std::string f = s.substr(0, isplit);
         if (f.length() > 0) {
            v.push_back(f);
         }
         if (s.length() >= (isplit+splitter.length())) {
            s = s.substr(isplit+splitter.length());
         } else {
            break;
         }
      } else {
         if (! s.empty()) {
            v.push_back(s);
         }
         break;
      }
   }
   return v;
}

float
from_string(const std::string &str) {

   float f;
   std::istringstream myStream(str);
   myStream >> f;
   return f;
}

void
parse_text(const std::string &command) {

   std::vector<std::string> parts = split_command(command);
   if (parts.size() == 1) {
      if (parts[0] == "spinthelights") {
         spin_the_lights();
      }
      if (parts[0] == "pumpkin") {
         import_a_pumpkin();
      }
   }
   if (parts.size() == 2) {
      if (parts[0] == "normals") {
         if (parts[1] == "on") {
            draw_normals(true);
         }
         if (parts[1] == "off") {
            draw_normals(false);
         }
      }
      if (parts[0] == "loadobj") {
#ifdef USE_ASSIMP
         display_info_t di;
         gtk_gl_area_attach_buffers(GTK_GL_AREA(di.gl_area));
         std::string s = parts[1];
         Model m(s);
         di.models.push_back(m);
#else
         std::cout << "loadobj not available - no assimp" << std::endl;
#endif
      }
   }
   if (parts.size() == 6) {
      if (parts[0] == "add") {
         if (parts[1] == "rep" || parts[1] == "representation") {
            std::string name          = parts[2];
            std::string selection     = parts[3];
            std::string colour_scheme = parts[4];
            std::string style         = parts[5];

            display_info_t di;
            gtk_gl_area_attach_buffers(GTK_GL_AREA(di.gl_area));
            Material material;
            molecular_mesh_generator_t mmg;
            std::vector<molecular_triangles_mesh_t> mtm =
               mmg.get_molecular_triangles_mesh(di.mol, selection, colour_scheme, style);
            Shader dummy_shader;
            di.add_model(mtm, &dummy_shader, material);

            unsigned int idx = di.meshes.size() - 1;
            add_rep_to_representations_table(idx, name);
         }
         if (parts[1] == "atom") {
            if (parts[2] == "label") {
               try {
                  gtk_gl_area_attach_buffers(GTK_GL_AREA(display_info_t::gl_area));
                  int res_no = from_string(parts[4]);
                  std::string atom_name = parts[5];
                  int l1 = atom_name.length();
                  if (l1 == 1) atom_name = "  " + atom_name + " ";
                  if (l1 == 2) atom_name = " " + atom_name + " ";
                  if (l1 == 3) atom_name = " " + atom_name;
                  coot::atom_spec_t spec(parts[3], res_no, "", atom_name, "");
                  float g = 0.5;
                  glm::vec4 col(g, g, g, 1.0);
                  add_atom_label(spec, col, display_info_t::mol);
               }
               catch (const std::runtime_error &rte) {
                  std::cout << "rte " << rte.what() << std::endl;
               }
            }
         }
      }
   }

   if (parts.size() == 3) {
      if (parts[0] == "add") {
         if (parts[1] == "rep" || parts[1] == "representation") {
            if (parts[2] == "rainbow-ribbons") {
               std::string name = "rainbow-ribbons";
               display_info_t di;
               gtk_gl_area_attach_buffers(GTK_GL_AREA(di.gl_area));
               std::vector<mmdb::Chain *> chains = chains_in_mol(di.mol);
               Material material;
               material.shininess = 700;
               material.specular_strength = 0.5;
               molecular_mesh_generator_t mmg;
               std::string colour_scheme = "Ramp";
               std::string style = "Ribbon";
               for (unsigned int ich=0; ich<chains.size(); ich++) {
                  std::vector<molecular_triangles_mesh_t> mtm =
                     mmg.get_molecular_triangles_mesh(di.mol, "//", colour_scheme, style);
                  di.add_model(mtm, &molecular_triangles_shader, material);
               }
               add_this_rep_to_representations_table();
            }

            if (parts[2] == "neighbours") {
               quick_ligand_neighbours();
            }
         }
      }
      if (parts[0] == "writeobj") {
         display_info_t di;
         gtk_gl_area_attach_buffers(GTK_GL_AREA(di.gl_area));
         std::string s = parts[1];
         int idx = from_string(s);
         std::string fn = parts[2];
         di.write_model_as_obj(idx, fn);
      }
   }

   if (parts.size() == 4) {
      if (parts[0] == "set") {
         if (parts[1] == "framebuffer") {
            if (parts[2] == "scale") {
               try {
                  int s = from_string(parts[3]);
                  display_info_t di;
                  di.set_framebufffer_scale(s);
               }
               catch (const std::runtime_error &rte) {
                  std::cout << "rte " << rte.what() << std::endl;
               }
            }
         }
      }
   }

   if (parts.size() == 5) {
      if (parts[0] == "light") {
         if (parts[2] == "direction") {
            try {
               if (parts[3] == "x") {
                  float f = from_string(parts[4]);
                  glm::vec3 new_direction = display_info_t::lights[0].direction;
                  new_direction.x = f;
                  new_direction = glm::normalize(new_direction);
                  display_info_t::lights[0].direction = new_direction;
               }
               if (parts[3] == "y") {
                  float f = from_string(parts[4]);
                  glm::vec3 new_direction = display_info_t::lights[0].direction;
                  new_direction.y = f;
                  new_direction = glm::normalize(new_direction);
                  display_info_t::lights[0].direction = new_direction;
               }
               if (parts[3] == "z") {
                  float f = from_string(parts[4]);
                  glm::vec3 new_direction = display_info_t::lights[0].direction;
                  new_direction.z = f;
                  new_direction = glm::normalize(new_direction);
                  display_info_t::lights[0].direction = new_direction;
               }
            }
            catch (const std::runtime_error &rte) {
               std::cout << "rte " << rte.what() << std::endl;
            }
         }
      }
   }
   
   if (parts.size() == 8) {
      if (parts[0] == "add") {
         if (parts[1] == "H-bond") {
            try {
               gtk_gl_area_attach_buffers(GTK_GL_AREA(display_info_t::gl_area));
               std::string chain_id_1 = parts[2];
               int res_no_1 = from_string(parts[3]);
               std::string atom_name_1 = parts[4];
               std::string chain_id_2 = parts[5];
               int res_no_2 = from_string(parts[6]);
               std::string atom_name_2 = parts[7];

               int l1 = atom_name_1.length();
               int l2 = atom_name_2.length();
               if (l1 == 1) atom_name_1 = "  " + atom_name_1 + " ";
               if (l1 == 2) atom_name_1 = " " + atom_name_1 + " ";
               if (l1 == 3) atom_name_1 = " " + atom_name_1;
               if (l2 == 1) atom_name_2 = "  " + atom_name_2 + " ";
               if (l2 == 2) atom_name_2 = " " + atom_name_2 + " ";
               if (l2 == 3) atom_name_2 = " " + atom_name_2;

               std::cout << "l1: " << l1 << std::endl;
               std::cout << "making spec from " << chain_id_1 << " " << res_no_1
                         << " :" << atom_name_1 << ":" << std::endl;

               coot::atom_spec_t spec_1(chain_id_1, res_no_1, "", atom_name_1, "");
               coot::atom_spec_t spec_2(chain_id_2, res_no_2, "", atom_name_2, "");

               display_info_t di;
               add_H_bond(spec_1, spec_2, di.mol);
               
            }
            catch (const std::runtime_error &rte) {
               std::cout << "rte " << rte.what() << std::endl;
            }
         }
      }
   }

   display_info_t di;
   di.add_to_history(command);

   di.redraw();

}
