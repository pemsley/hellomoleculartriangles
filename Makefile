
# you will need to adjust these, I imagine.
GLM=$(HOME)/glm
MMDB=$(HOME)/autobuild/Linux-penelope-pre-release-gtk3-python
MOLTRI=$(HOME)/autobuild/Linux-penelope-pre-release-gtk3-python
# pybind11 is installed by pip for this python:
PYTHON=$(HOME)/python3
PYTHON_VERSION=python3.8

# ASSIMP=$(HOME)/assimp

# pkg-config --cflags includedir (assimp.pc) was set incorrectly on assimp install!

CXXFLAGS= \
   `pkg-config --cflags gtk+-3.0` `pkg-config --cflags epoxy`  \
   -g -O0 -march=native -std=gnu++11 \
   -DTHIS_IS_HMT \
   -I$(GLM)/include -I$(MMDB)/include \
   -I$(MOLTRI)/include/MoleculesToTriangles -DUSE_MOLECULES_TO_TRIANGLES \
   -I$(PYTHON)/include/$(PYTHON_VERSION)

# no assimp for now
# `pkg-config --cflags assimp`

# put the rpaths on separate lines - maybe helps with merging?
# mmdb2 does work with pkg-config (!?) - is that what I meant?

# This needs PKG_CONFIG_PATH to be set correctly.
# setenv PKG_CONFIG_PATH $BUILD/lib/pkgconfig:$BUILD/lib/pkgconfig or some such
#
LDLIBS=`pkg-config --libs gtk+-3.0` \
   `pkg-config --libs epoxy` \
   `pkg-config --libs MoleculesToTrianglesCXXClasses` \
   `pkg-config --libs MoleculesToTrianglesCXXSurface` \
   `pkg-config --libs mmdb2` \
   `pkg-config --libs python-3.8-embed` \
   -lfreetype \
   -lm

# No assimp for now
#   `pkg-config --libs assimp`

EXPORT_DYNAMIC_FLAG_SPEC=-Wl,--export-dynamic
# Hmm... How do we do this on the Mac? Make it blank, I think.
GLADE_LDFLAGS = $(EXPORT_DYNAMIC_FLAG_SPEC)

hmt-glade: hmt-glade.o hmt-callbacks.o \
	hmt.o Shader.o Mesh.o dodec.o cylinder.o molecular-mesh-generator.o oct.o prideout-octasphere.o \
	utils.o Particle.o framebuffer.o molecular-mesh-generator-mol-tris.o display-info-statics.o \
	python-bindings.o coot-coord-utils.o text-rendering-utils.o Texture.o stb_image.o \
	TextureMesh.o display-info.o obj_loader.o screendump-tga.o parse.o \
	Model.o pumpkin.o cylinder-with-rotation-translation.o
	$(CXX) $^ $(CXXFLAGS) -o $@ $(LDLIBS) $(GLADE_LDFLAGS)

hmt-simple: hmt-simple.o hmt.o Shader.o Mesh.o dodec.o cylinder.o molecular-mesh-generator.o oct.o \
	prideout-octasphere.o utils.o Particle.o framebuffer.o molecular-mesh-generator-mol-tris.o \
	hmt-statics.o python-bindings.o coot-coord-utils.o
	$(CXX) $^ $(CXXFLAGS) -o $@ $(LDLIBS)

hmt.o: hmt.cc mouse-button-info.hh display-info.hh

display-info-statics.o: display-info-statics.cc display-info.hh

obj-loader.o: obj-loader.cc obj-loader.h

screendump-tga.o: screendump-tga.cc

parse.o: parse.cc

pumpkin.o: pumpkin.cc pumpkin.hh

coot-coord-utils.o: coot-coord-utils.cc coot-coord-utils.hh

residue-and-atom-specs.o: residue-and-atom-specs.cc

Shader.o: Shader.cc Shader.hh lights-info.hh display-info.hh

hmt-callbacks.o: hmt-callbacks.cc display-info.hh

Mesh.o: Mesh.cc Mesh.hh cylinder.hh generic-display-object.hh display-info.hh

Particle.o: Particle.cc Particle.hh display-info.hh

Texture.o: Texture.cc Texture.hh

Model.o: Model.cc Model.hh

TextureMesh.o: TextureMesh.cc TextureMesh.hh

dodec.o: dodec.cc

display-info.o: display-info.cc display-info.hh

cylinder.o: cylinder.cc cylinder.hh generic-vertex.hh g_triangle.hh

stb_image.o: stb_image.c stb_image.h

oct.o: oct.cc prideout-octasphere.hh g_triangle.hh

utils.o: utils.cc utils.hh

framebuffer.o: framebuffer.cc framebuffer.hh

text-rendering-utils.o: text-rendering-utils.cc text-rendering-utils.hh

python-binding.o: python-bindings.cc
	$(CXX) $(CXXFLAGS) -shared -fPIC `python3 -m pybind11 --includes` \
	$^ -o hmt`python3-config --extension-suffix` $(LDLIBS)

molecular-mesh-generator.o: molecular-mesh-generator.cc molecular-mesh-generator.hh \
	lights-info.hh generic-vertex.hh

molecular-mesh-generator-mol-tris.o: molecular-mesh-generator-mol-tris.cc molecular-mesh-generator.hh \
	lights-info.hh generic-vertex.hh

perspective-trackball: perspective-trackball.cc Shader.cc
	$(CXX) $^ $(CXXFLAGS) -o $@ $(LDLIBS)

tar:
	tar cvzf hello-molecular-triangles.tar.gz *.cc *.hh Makefile *.pdb *.shader utils/coot-utils.hh utils/dodec.hh

# not used
%: %.cc
	$(LINK.cc) -o $@ $^ $(LOADLIBES) $(LDLIBS)

clean:
	rm -f *.o hmt

b-spline: b-spline.cc

