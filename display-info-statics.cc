
// mmdb
#include <mmdb2/mmdb_manager.h>

#include <math.h>
#include <gtk/gtk.h>
#include <epoxy/gl.h>
#ifndef __APPLE__
#include <epoxy/glx.h>
#endif

#include "display-info.hh"

framebuffer display_info_t::framebuffer_for_screen;
bool display_info_t::use_framebuffer_for_screen = true;
unsigned int display_info_t::framebuffer_scale = 1; // no supersampling by default

glm::vec3 display_info_t::bg_col = glm::vec3(0.2, 0.2, 0.2);
GtkWidget *display_info_t::main_application = 0;
GtkWidget *display_info_t::gl_area = 0;
GtkWidget *display_info_t::rep_box = 0;
mmdb::Manager *display_info_t::mol = 0;
std::map<GLchar, FT_character> display_info_t::ft_characters;
bool display_info_t::vera_font_loaded = false;

bool display_info_t::do_ambient_occlusion = true;
bool display_info_t::do_normals = false;

std::vector<std::string> display_info_t::command_history;

std::map<unsigned int, lights_info_t> display_info_t::lights;

glm::vec3 display_info_t::rotation_centre = glm::vec3(0.0, 0.0, 0.0);

bool display_info_t::do_depth_fog = false;
bool display_info_t::do_outline   = false;

int display_info_t::command_history_index = 0;

std::vector<Mesh> display_info_t::meshes;
std::vector<TextureMesh> display_info_t::texture_meshes;
std::map<std::string,Texture> display_info_t::textures;
std::vector<atom_label_info_t> display_info_t::atom_labels;
std::vector<Model> display_info_t::models;
