
// use the #shader "directive" to separate the shaders on parsing
// -----------------------------------------
#shader vertex

#version 330 core

layout(location = 0) in vec3 position;
layout(location = 1) in vec3 normal;
layout(location = 2) in vec4 colour;

uniform mat4 mvp;
out vec4 colour_transfer;

void main() {
   colour_transfer = colour;
   gl_Position = mvp * vec4(position, 1.0);
}


// -----------------------------------------
#shader fragment

#version 330 core

struct Material {
    vec4 emission;
    vec4 ambient;
    vec4 diffuse;
    vec4 specular;
    float shininess;
};
uniform Material material;

struct LightSource {
    vec4 ambient;
    vec4 diffuse;
    vec4 specular;
    vec4 position;
    vec4 halfVector;
    vec3 spotDirection;
    float spotExponent;
    float spotCutoff;
    float spotCosCutoff;
    float constantAttenuation;
    float linearAttenuation;
    float quadraticAttenuation;
};

uniform LightSource light_sources[2];

in vec4 colour_transfer;

out vec4 outputColor;

void main() {

   outputColor = colour_transfer;
}

