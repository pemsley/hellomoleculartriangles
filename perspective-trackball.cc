/* OpenGL Area
 *
 * GtkGLArea is a widget that allows custom drawing using OpenGL calls.
 */

#include <string>
#include <fstream>
#include <iostream>
#include <memory>

// mmdb
#include <mmdb2/mmdb_manager.h>

#include <math.h>
#include <gtk/gtk.h>
#include <epoxy/gl.h>
#include <epoxy/glx.h>

// GLM
#include <glm/ext.hpp>
#define GLM_ENABLE_EXPERIMENTAL // # for norm things
#include <glm/gtx/string_cast.hpp>  // to_string()

#include <MoleculesToTriangles/CXXClasses/MolecularRepresentation.h>
#include <MoleculesToTriangles/CXXClasses/MolecularRepresentationInstance.h>

#include "graphical-molecule.hh"
#include "Shader.hh" // real coot code
#include "lights-info.hh"

#include "mouse-button-info.hh" // contains accumulating mouse-based orientation quaternion

graphical_molecule gm; // contains vertices, triangle indices, as setup function and a draw funtion.
mouse_button_info_t mouse_button_info;

static GtkWidget *demo_window = NULL;

/* the GtkGLArea widget */
static GtkWidget *gl_area = NULL;

enum {
  X_AXIS,
  Y_AXIS,
  Z_AXIS,

  N_AXIS
};

Shader hello_shader;
Shader molecular_triangles_shader;

GLuint simple_triangles_vao;

// put these in a class, I think - graphical-molecule?
//
// GLuint simple_triangles_position_buffer_id;
// GLuint simple_triangles_normal_buffer_id;
// GLuint simple_triangles_colour_buffer_id;
GLuint simple_triangles_buffer_id;
GLuint simple_triangles_index_buffer_id;

GLuint simple_triangles_program;
// GLuint mvp_location;

std::map<unsigned int, gl_lights_info_t> lights;

/* Rotation angles on each axis */
float rotation_angles[N_AXIS] = { 0.0 };


void
init_vertex_array(GLuint *vao_p) {

   /* We only use one VAO (for this simple case), so we always keep it bound */

   glGenVertexArrays (1, vao_p);
   glBindVertexArray (*vao_p);

}

void
graphical_molecule::fill_with_direction_triangles() {

   vertices.resize(2 * 3);

   vertices[0].pos = glm::vec3(  0.f,   0.5f,  -0.2f);
   vertices[1].pos = glm::vec3(  0.5f, -0.36f, -0.2f);
   vertices[2].pos = glm::vec3( -0.5f, -0.36f, -0.2f);
   vertices[3].pos = glm::vec3(  0.0f,  0.5f,   0.2f);
   vertices[4].pos = glm::vec3(  0.5f, -0.36f,  0.2f);
   vertices[5].pos = glm::vec3( -0.5f, -0.36f,  0.2f);

   vertices[0].normal = glm::vec3( 0.2f, 0.2f,  0.9f);
   vertices[1].normal = glm::vec3( 0.2f, 0.9f,  0.2f);
   vertices[2].normal = glm::vec3( 0.9f, 0.3f,  0.1f);
   vertices[3].normal = glm::vec3( 0.0f, 0.9f, -0.1f);
   vertices[4].normal = glm::vec3( 0.9f, 0.3f, -0.2f);
   vertices[5].normal = glm::vec3( 0.2f, 0.6f, -0.9f);

   vertices[0].color = glm::vec4(0.0f, 0.0f, 0.0f, 1.f);
   vertices[1].color = glm::vec4(0.2f, 0.3f, 1.0f, 1.f);
   vertices[2].color = glm::vec4(0.5f, 0.9f, 0.2f, 1.f);
   vertices[3].color = glm::vec4(0.2f, 0.2f, 0.9f, 1.f);
   vertices[4].color = glm::vec4(0.1f, 0.9f, 0.2f, 1.f);
   vertices[5].color = glm::vec4(0.9f, 0.3f, 0.2f, 1.f);

   graphical_triangle gt_0(0,1,2);
   graphical_triangle gt_1(3,4,5);
   triangle_vertex_indices.push_back(gt_0);
   triangle_vertex_indices.push_back(gt_1);

}

void
graphical_molecule::fill_with_simple_triangles_vertices() {

   vertices.resize(2 * 3);

   vertices[0].pos = glm::vec3(  0.f,   0.5f,  -0.2f);
   vertices[1].pos = glm::vec3(  0.5f, -0.36f, -0.2f);
   vertices[2].pos = glm::vec3( -0.5f, -0.36f, -0.2f);
   vertices[3].pos = glm::vec3(  0.0f,  0.5f,   0.2f);
   vertices[4].pos = glm::vec3(  0.5f, -0.36f,  0.2f);
   vertices[5].pos = glm::vec3( -0.5f, -0.36f,  0.2f);

   vertices[0].normal = glm::vec3( 0.2f, 0.2f,  0.9f);
   vertices[1].normal = glm::vec3( 0.2f, 0.9f,  0.2f);
   vertices[2].normal = glm::vec3( 0.9f, 0.3f,  0.1f);
   vertices[3].normal = glm::vec3( 0.0f, 0.9f, -0.1f);
   vertices[4].normal = glm::vec3( 0.9f, 0.3f, -0.2f);
   vertices[5].normal = glm::vec3( 0.2f, 0.6f, -0.9f);

   vertices[0].color = glm::vec4(0.0f, 0.0f, 0.0f, 1.f);
   vertices[1].color = glm::vec4(0.2f, 0.3f, 1.0f, 1.f);
   vertices[2].color = glm::vec4(0.5f, 0.9f, 0.2f, 1.f);
   vertices[3].color = glm::vec4(0.2f, 0.2f, 0.9f, 1.f);
   vertices[4].color = glm::vec4(0.1f, 0.9f, 0.2f, 1.f);
   vertices[5].color = glm::vec4(0.9f, 0.3f, 0.2f, 1.f);

   graphical_triangle gt_0(0,1,2);
   graphical_triangle gt_1(3,4,5);
   triangle_vertex_indices.push_back(gt_0);
   triangle_vertex_indices.push_back(gt_1);

}


void
init_shaders() {

   hello_shader.init("hello.shader", Shader::Entity_t::GENERIC_DISPLAY_OBJECT); // not MODEL

}


void
init_lights() {

   gl_lights_info_t light;
   light.position = glm::vec4(-2.0f, -1.0f, 5.0f, 1.0f);
   lights[0] = light;
   light.position = glm::vec4( 3.0f, -2.0f, 4.0f, 1.0f);
   lights[1] = light;
}

void
graphical_molecule::setup_simple_triangles () {

   GLenum err = glGetError();
   if (err) std::cout << "error:: draw_simple_triangles -- start -- "
                      << err << std::endl;

   hello_shader.Use();

   fill_with_simple_triangles_vertices();

   glGenVertexArrays (1, &simple_triangles_vao);
   glBindVertexArray (simple_triangles_vao);

   glGenBuffers(1, &simple_triangles_buffer_id);
   glBindBuffer(GL_ARRAY_BUFFER, simple_triangles_buffer_id);
   std::cout << "debug:: sizeof(gm.vertices) " << sizeof(gm.vertices) << std::endl;
   std::cout << "debug:: sizeof(gm.vertices[0]) " << sizeof(gm.vertices[0]) << std::endl;
   std::cout << "debug:: sizeof(generic_vertex) " << sizeof(generic_vertex) << std::endl;
   unsigned int n_vertices = gm.vertices.size();
   std::cout << "debug:: glBufferData for simple_triangles_buffer_id " << simple_triangles_buffer_id
             << " allocating with size " << n_vertices * sizeof(gm.vertices[0]) << std::endl;
   glBufferData(GL_ARRAY_BUFFER, n_vertices * sizeof(gm.vertices[0]), &(gm.vertices[0]), GL_STATIC_DRAW);
   glEnableVertexAttribArray(0);
   glVertexAttribPointer (0, 3, GL_FLOAT, GL_FALSE, sizeof(generic_vertex), 0);
                          
   glEnableVertexAttribArray (1);
   glVertexAttribPointer (1, 3, GL_FLOAT, GL_FALSE, sizeof(generic_vertex),
                          reinterpret_cast<void *>(sizeof(glm::vec3)));

   glEnableVertexAttribArray (2);
   glVertexAttribPointer (2, 4, GL_FLOAT, GL_FALSE, sizeof(generic_vertex),
                          reinterpret_cast<void *>(2 * sizeof(glm::vec3)));

   glGenBuffers(1, &simple_triangles_index_buffer_id);
   err = glGetError(); if (err) std::cout << "GL error setup_simple_triangles()\n";
   glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, simple_triangles_index_buffer_id);
   err = glGetError(); if (err) std::cout << "GL error setup_simple_triangles()\n";
   unsigned int n_bytes = 2 * 3 * sizeof(unsigned int);
   glBufferData(GL_ELEMENT_ARRAY_BUFFER, n_bytes, &gm.triangle_vertex_indices[0], GL_STATIC_DRAW);
   err = glGetError(); if (err) std::cout << "GL error setup_simple_triangles()\n";

   glDisableVertexAttribArray (0);
   glBindBuffer(GL_ARRAY_BUFFER, 0);
   glUseProgram(0);
}

void
setup_molecular_triangles(mmdb::Manager *mol) {

   // something or other... Anything that will generate molecular triangles.

   auto my_mol = std::make_shared<MyMolecule>(mol);
   auto ss_cs = ColorScheme::colorBySecondaryScheme();

   int imodel = 1;
   mmdb::Model *model_p = mol->GetModel(imodel);
   if (model_p) {
      auto this_cs = ss_cs;
      std::string atom_selection_str = "/1";
      std::string colour_scheme = "colorRampChainsScheme";
      std::string style = "Ribbon";
      std::shared_ptr<MolecularRepresentationInstance> molrepinst_1 =
         MolecularRepresentationInstance::create(my_mol, this_cs, atom_selection_str, style);

      // create a graphical_molecule that has vertices and triangle vertex indicies.

   }
}


void my_glarea_add_other_signals_and_events(GtkWidget *glarea); // use a header

/* We need to set up our state when we realize the GtkGLArea widget */
void
realize (GtkWidget *widget) {

   gtk_gl_area_make_current (GTK_GL_AREA (widget));
   if (gtk_gl_area_get_error (GTK_GL_AREA (widget)) != NULL)
      return;

   gtk_gl_area_set_has_depth_buffer(GTK_GL_AREA(widget), TRUE);
   glEnable(GL_DEPTH_TEST);

   init_lights();

   init_shaders();

   GdkGLContext *context = gtk_gl_area_get_context (GTK_GL_AREA (widget));
   gm.setup_simple_triangles();

}

/* We should tear down the state when unrealizing */
void
unrealize (GtkWidget *widget) {

   gtk_gl_area_make_current (GTK_GL_AREA (widget));

   if (gtk_gl_area_get_error (GTK_GL_AREA (widget)) != NULL)
      return;

   glDeleteBuffers (1, &simple_triangles_buffer_id);
   glDeleteBuffers (1, &simple_triangles_vao);
   glDeleteProgram (simple_triangles_program);
}

glm::mat4 get_mvp() {

   glm::mat4 model_matrix(1.0);
   glm::mat4 view_matrix(1.0);

   if (true) {
      model_matrix = glm::rotate(model_matrix, 0.009f * rotation_angles[0], glm::vec3(1,0,0));
      model_matrix = glm::rotate(model_matrix, 0.009f * rotation_angles[1], glm::vec3(0,1,0));
      model_matrix = glm::rotate(model_matrix, 0.009f * rotation_angles[2], glm::vec3(0,0,1));
   }

   glm::mat4 quat_mat(mouse_button_info.quat);
   model_matrix = quat_mat * model_matrix;

   double near = -2.0f;
   double far  =  2.0f;
   glm::mat4 proj = glm::ortho(-1.0, 1.0, -1.0, 1.0, near, far);
   glm::mat4 mvp = proj * view_matrix * model_matrix;

   bool use_perspective_projection = true;

   if (use_perspective_projection) {
      float fov = 35.0; // degrees, the smaller this value, the more we seem to be
                        // "zoomed in."
      float screen_ratio = 1.0;
      float z_front = 0.1;
      float z_back = 10.0;
      glm::vec3 ep(0,0,2);
      glm::vec3 rc(0,0,0);
      glm::vec3 up(0,1,0);
      view_matrix = glm::lookAt(ep, rc, up);
      proj = glm::perspective(glm::radians(fov), screen_ratio, z_front, z_back);
      mvp = proj * view_matrix * model_matrix;
   }

   return mvp;

}


void
graphical_molecule::draw_simple_triangles () {

   hello_shader.Use();
   glm::mat4 mvp = get_mvp();
   glUniformMatrix4fv(hello_shader.mvp_uniform_location, 1, GL_FALSE, &mvp[0][0]);

   // lights
   std::map<unsigned int, gl_lights_info_t>::const_iterator it;
   it = lights.find(0);
   if (it != lights.end()) {
      const gl_lights_info_t &light = it->second;
      glUniform1i(hello_shader.light_0_is_on_uniform_location, light.is_on);
      glUniform4fv(hello_shader.light_0_position_uniform_location, 1, glm::value_ptr(light.position));
   }
   it = lights.find(1);
   if (it != lights.end()) {
      const gl_lights_info_t &light = it->second;
      glUniform1i(hello_shader.light_1_is_on_uniform_location, light.is_on);
      glUniform4fv(hello_shader.light_1_position_uniform_location, 1, glm::value_ptr(light.position));
   }

   glBindVertexArray(simple_triangles_vao);
   GLenum  err = glGetError();
   if (err) std::cout << "   error glBindVertexArray() " << simple_triangles_vao
                      << " with GL err " << err << std::endl;

   glBindBuffer(GL_ARRAY_BUFFER, simple_triangles_buffer_id);
   err = glGetError(); if (err) std::cout << "   error draw_model_molecules() glBindBuffer() v "
                                          << err << std::endl;
   glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, simple_triangles_index_buffer_id);
   err = glGetError(); if (err) std::cout << "   error draw_model_molecules() glBindBuffer() i "
                                          << err << std::endl;

   glEnableVertexAttribArray(0);
   glEnableVertexAttribArray(1);
   glEnableVertexAttribArray(2);
   unsigned int n_triangles = 2;
   GLuint n_verts = 3 * n_triangles;
   glDrawElements(GL_TRIANGLES, n_verts, GL_UNSIGNED_INT, nullptr);

   glDisableVertexAttribArray (0);
   glDisableVertexAttribArray (1);
   glDisableVertexAttribArray (2);
   glBindBuffer (GL_ARRAY_BUFFER, 0);
   glUseProgram (0);

}


void
draw_molecular_triangles() {

   molecular_triangles_shader.Use();

   // molecular triangular magicular here

   // (will need to either scaled these down to match the size of the
   // simple_triangles - or change the projection matrix)

   glDisableVertexAttribArray (0);
   glBindBuffer (GL_ARRAY_BUFFER, 0);
   glUseProgram (0);
}

static gboolean
render (GtkGLArea    *area,
        GdkGLContext *context) {

   if (gtk_gl_area_get_error (area) != NULL)
    return FALSE;

   glClearColor (0.2, 0.2, 0.2, 1.0);
   glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

   gm.draw_simple_triangles();

   // this should be part of gm now.
   // draw_molecular_triangles();

   glFlush ();

   return TRUE;
}

static void
on_axis_value_change (GtkAdjustment *adjustment,
                      gpointer       data)
{
  int axis = GPOINTER_TO_INT (data);
  g_assert (axis >= 0 && axis < N_AXIS);
  rotation_angles[axis] = 2.0 * gtk_adjustment_get_value (adjustment);
  gtk_widget_queue_draw (gl_area);
}

static GtkWidget *
create_axis_slider (int axis)
{
  GtkWidget *box, *label, *slider;
  GtkAdjustment *adj;
  const char *text;

  box = gtk_box_new (GTK_ORIENTATION_HORIZONTAL, 0);

  switch (axis)
    {
    case X_AXIS:
      text = "X axis";
      break;

    case Y_AXIS:
      text = "Y axis";
      break;

    case Z_AXIS:
      text = "Z axis";
      break;

    default:
      g_assert_not_reached ();
    }

  label = gtk_label_new (text);
  gtk_container_add (GTK_CONTAINER (box), label);
  gtk_widget_show (label);

  adj = gtk_adjustment_new (0.0, 0.0, 360.0, 1.0, 12.0, 0.0);
  g_signal_connect (adj, "value-changed",
                    G_CALLBACK (on_axis_value_change),
                    GINT_TO_POINTER (axis));
  slider = gtk_scale_new (GTK_ORIENTATION_HORIZONTAL, adj);
  gtk_container_add (GTK_CONTAINER (box), slider);
  gtk_widget_set_hexpand (slider, TRUE);
  gtk_widget_show (slider);

  gtk_widget_show (box);

  return box;
}

static void
close_window (GtkWidget *widget)
{

  demo_window = NULL;
  gl_area = NULL;

  rotation_angles[X_AXIS] = 0.0;
  rotation_angles[Y_AXIS] = 0.0;
  rotation_angles[Z_AXIS] = 0.0;
}

GtkWidget *
create_glarea_window () {

   GtkWidget *window, *box, *button;
   int i;

   window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
   demo_window = window;

   // gtk_window_set_screen (GTK_WINDOW (window), gtk_widget_get_screen (do_widget));

   gtk_window_set_title (GTK_WINDOW (window), "Hello Molecular Triangles");
   gtk_window_set_default_size (GTK_WINDOW (window), 400, 600);
   gtk_container_set_border_width (GTK_CONTAINER (window), 12);
   g_signal_connect (window, "destroy", G_CALLBACK (close_window), NULL);

   box = gtk_box_new (GTK_ORIENTATION_VERTICAL, FALSE);
   gtk_box_set_spacing (GTK_BOX (box), 6);
   gtk_container_add (GTK_CONTAINER (window), box);

   gl_area = gtk_gl_area_new(); // gl_area is global
   gtk_widget_set_hexpand (gl_area, TRUE);
   gtk_widget_set_vexpand (gl_area, TRUE);
   gtk_container_add (GTK_CONTAINER (box), gl_area);

   /* We need to initialize and free GL resources, so we use
    * the realize and unrealize signals on the widget
    */
   g_signal_connect (gl_area, "realize",   G_CALLBACK(realize),   NULL);
   g_signal_connect (gl_area, "unrealize", G_CALLBACK(unrealize), NULL);
   /* The main "draw" call for GtkGLArea */
   g_signal_connect (gl_area, "render", G_CALLBACK (render), NULL);

   my_glarea_add_other_signals_and_events(gl_area); // mouse and keyboard

   GtkWidget *controls_vbox = gtk_box_new (GTK_ORIENTATION_VERTICAL, FALSE);
   gtk_container_add (GTK_CONTAINER (box), controls_vbox);
   gtk_widget_set_hexpand (controls_vbox, TRUE);

   for (i = 0; i < N_AXIS; i++)
      gtk_container_add (GTK_CONTAINER (controls_vbox), create_axis_slider (i));

   button = gtk_button_new_with_label ("Quit");
   gtk_widget_set_hexpand (button, TRUE);
   gtk_container_add (GTK_CONTAINER (box), button);
   g_signal_connect (button, "clicked", G_CALLBACK (gtk_main_quit), window);

   return window;
}

GtkWidget*
do_glarea () {

   if (demo_window == NULL) {
      demo_window = create_glarea_window();
   }

   if (!gtk_widget_get_visible (demo_window))
      gtk_widget_show_all(demo_window);
   else
      gtk_widget_destroy (demo_window);

   return demo_window;
}

int main(int argc, char **argv) {

  int r = 0;
  GtkWidget *window;
  GtkWidget *vbox;

  gtk_init(&argc, &argv);

  if (argc > 1) {
     std::string file_name = argv[1];
     mmdb::Manager *mol = new mmdb::Manager;
     // PDB file with molecule at/near the origin
     mmdb::ERROR_CODE err = mol->ReadCoorFile(file_name.c_str());
     if (err == mmdb::Error_Ok) {
        setup_molecular_triangles(mol);
     }
  }

  do_glarea();
  gtk_main ();

  return r;

}


gboolean
on_glarea_scroll(GtkWidget *widget, GdkEventScroll *event) {
   return FALSE;
}

gboolean
on_glarea_button_press(GtkWidget *widget, GdkEventButton *event) {
   mouse_button_info.add_press(event->x, event->y);
   return TRUE;
}

gboolean
on_glarea_button_release(GtkWidget *widget, GdkEventButton *event) {

   mouse_button_info.clear_press();
   return TRUE;
}

float
trackball_project_to_sphere(float r, float x, float y) {

    float oor2 = 0.70710678118654752440;
    float d = sqrt(x*x + y*y);
    if (d < r * oor2) {    /* Inside sphere */
        return sqrt(r*r - d*d);
    } else {
       /* On hyperbola */
       float t = r * oor2;
       return t*t/d;
    }
}

glm::quat
trackball_to_quaternion(float p1x, float p1y, float p2x, float p2y, float trackball_size) {

   if (p1x == p2x && p1y == p2y) {
        /* Zero rotation */
        return glm::quat(1,0,0,0);
    }

    float d_mult = trackball_size - 0.3; /* or some such */

    /*
     * First, figure out z-coordinates for projection of P1 and P2 to
     * deformed sphere
     */
    glm::vec3 p1(p1x, p1y, trackball_project_to_sphere(trackball_size, p1x, p1y));
    glm::vec3 p2(p2x, p2y, trackball_project_to_sphere(trackball_size, p2x, p2y));

    /*
     *  Now, we want the cross product of P1 and P2
     */
    // vcross(p2,p1,a);
    glm::vec3 a = glm::normalize(glm::cross(p2,p1));

    /*
     *  Figure out how much to rotate around that axis.
     */
    glm::vec3 d(p1 - p2);

    float t = glm::length(d) * d_mult / (trackball_size);

    /*
     * Avoid problems with out-of-control values...
     */
    if (t >  1.0f) t =  1.0f;
    if (t < -1.0f) t = -1.0f;

    /* how much to rotate about axis */
    float phi = 2.0f * asin(t);

    glm::vec3 qaaa(a * sinf(phi/2.0f));
    glm::quat q(cosf(phi/2.0f), qaaa.x, qaaa.y, qaaa.z);
    return q;
}

gboolean
on_glarea_motion_notify(GtkWidget *widget, GdkEventMotion *event) {

   if (mouse_button_info.is_pressed) {

      if (false)
         std::cout << "delta from previous: "
                   << event->x - mouse_button_info.previous_at_x << " "
                   << event->y - mouse_button_info.previous_at_y << std::endl;

      GtkAllocation allocation;
      gtk_widget_get_allocation(gl_area, &allocation);
      int w = allocation.width;
      int h = allocation.height;
      float tbs = 10.0;
      
      glm::quat tb_quat =
         trackball_to_quaternion((2.0*mouse_button_info.previous_at_x - w)/w,
                                 (h - 2.0*mouse_button_info.previous_at_y)/h,
                                 (2.0*event->x - w)/w,
                                 (h - 2.0*event->y)/h,
                                 tbs);

      glm::quat tb_transpose = glm::conjugate(tb_quat);
      mouse_button_info.add_rotation_delta(tb_transpose);

      // for next time
      mouse_button_info.add_previous(event->x, event->y);
      gtk_widget_queue_draw (gl_area);
   }
   return TRUE;
}


gboolean
on_glarea_key_press_notify(GtkWidget *widget, GdkEventKey *event) {
   return FALSE;
}

gboolean
on_glarea_key_release_notify(GtkWidget *widget, GdkEventKey *event) {
   return FALSE;
}




void
my_glarea_add_other_signals_and_events(GtkWidget *glarea) {

   gtk_widget_add_events(glarea, GDK_SCROLL_MASK);
   gtk_widget_add_events(glarea, GDK_BUTTON_PRESS_MASK);
   gtk_widget_add_events(glarea, GDK_BUTTON_RELEASE_MASK);
   gtk_widget_add_events(glarea, GDK_BUTTON1_MOTION_MASK);
   gtk_widget_add_events(glarea, GDK_BUTTON2_MOTION_MASK);
   gtk_widget_add_events(glarea, GDK_BUTTON3_MOTION_MASK);
   gtk_widget_add_events(glarea, GDK_POINTER_MOTION_MASK);
   gtk_widget_add_events(glarea, GDK_BUTTON1_MASK);
   gtk_widget_add_events(glarea, GDK_BUTTON2_MASK);
   gtk_widget_add_events(glarea, GDK_BUTTON3_MASK);
   gtk_widget_add_events(glarea, GDK_KEY_PRESS_MASK);

   // key presses for the glarea:

   gtk_widget_set_can_focus(glarea, TRUE);
   gtk_widget_grab_focus(glarea);

   g_signal_connect(glarea, "scroll-event",          G_CALLBACK(on_glarea_scroll),             NULL);
   g_signal_connect(glarea, "button-press-event",    G_CALLBACK(on_glarea_button_press),       NULL);
   g_signal_connect(glarea, "button-release-event",  G_CALLBACK(on_glarea_button_release),     NULL);
   g_signal_connect(glarea, "motion-notify-event",   G_CALLBACK(on_glarea_motion_notify),      NULL);
   g_signal_connect(glarea, "key-press-event",       G_CALLBACK(on_glarea_key_press_notify),   NULL);
   g_signal_connect(glarea, "key-release-event",     G_CALLBACK(on_glarea_key_release_notify), NULL);

}
