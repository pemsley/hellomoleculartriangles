
#include <string>
#include <iostream>

// mmdb
#include <mmdb2/mmdb_manager.h> // because "Success"

#include <math.h>
#include <gtk/gtk.h>
#include <epoxy/gl.h>
#ifndef __APPLE__
#include <epoxy/glx.h>
#endif

#include "display-info.hh"
#include "hmt.hh"


void parse_text(const std::string &t);

extern "C" G_MODULE_EXPORT void
on_quit_button_clicked(GtkButton *button, gpointer user_data) {
   display_info_t::write_history();
   gtk_main_quit();
}

extern "C" G_MODULE_EXPORT
void
on_main_application_destroy(GtkWidget *w, gpointer user_data) {
   gtk_main_quit();
}

extern "C" G_MODULE_EXPORT
void
on_file_quit_menu_item_activate(GtkMenuItem *menuitem,
                                gpointer     user_data) {
   gtk_main_quit();
}

extern "C" G_MODULE_EXPORT
void
on_file_open_menu_item_activate(GtkMenuItem *menuitem,
                                gpointer     user_data) {

   // may not be the best way to cast
   GtkWidget *application_window = GTK_WIDGET(user_data);
   std::cout << "here with application_window " << application_window << std::endl;
   GtkWidget *file_chooser = GTK_WIDGET(g_object_get_data(G_OBJECT(application_window), "file_chooser"));
   std::cout << "here with file_chooser " << file_chooser << std::endl;
   gtk_widget_show(file_chooser);

   // Check return value from Open Text File dialog box to see if user clicked the Open button
   if (gtk_dialog_run(GTK_DIALOG (file_chooser)) == GTK_RESPONSE_OK) {

      std::cout << "got repsonse OK! " << std::endl;

      // Get the file name from the dialog box
      char *file_name_c = gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(file_chooser));
      if (file_name_c) {
         std::string file_name(file_name_c);
         import_molecule_from_file(file_name);
         gtk_widget_hide(file_chooser);
      }
      g_free(file_name_c);
   }
}

extern "C" G_MODULE_EXPORT
void
on_command_entry_activate(GtkEntry *entry, gpointer  user_data) {

   std::string t = gtk_entry_get_text(entry);
   parse_text(t);
   gtk_entry_set_text(entry, "");
   
}

extern "C" G_MODULE_EXPORT
void
on_command_entry_changed(GtkEntry *entry, gchar *preedit, gpointer  user_data) {

   std::cout << "__________________ changed " << entry << std::endl;
   std::string t = gtk_entry_get_text(entry);

}

extern "C" G_MODULE_EXPORT
gboolean
on_command_entry_key_press_event (GtkWidget   *entry,
                                  GdkEventKey *event,
                                  gpointer     user_data) {

   if (event->keyval == GDK_KEY_Up) {
      display_info_t di;
      std::string t = di.get_previous_command();
      gtk_entry_set_text(GTK_ENTRY(entry), t.c_str());
      return TRUE;
   }
   if (event->keyval == GDK_KEY_Down) {
      display_info_t di;
      std::string t = di.get_next_command();
      gtk_entry_set_text(GTK_ENTRY(entry), t.c_str());
      return TRUE;
   }
   
   return FALSE;
}

extern "C" G_MODULE_EXPORT void
on_quick_ligands_button_clicked(GtkButton *button, gpointer user_data) {
   quick_ligands();
   gtk_widget_queue_draw(display_info_t::gl_area);
}

extern "C" G_MODULE_EXPORT void
on_quick_waters_button_clicked(GtkButton *button, gpointer user_data) {
   quick_waters();
   gtk_widget_queue_draw(display_info_t::gl_area);
}

extern "C" G_MODULE_EXPORT void
on_background_white_button_clicked(GtkButton *button, gpointer user_data) {
   display_info_t::bg_col = glm::vec3(1.0, 1.0, 1.0);
   gtk_widget_queue_draw(display_info_t::gl_area);
   
}

extern "C" G_MODULE_EXPORT void
on_background_black_button_clicked(GtkButton *button, gpointer user_data) {
   display_info_t::bg_col = glm::vec3(0.0, 0.0, 0.0);
   gtk_widget_queue_draw(display_info_t::gl_area);

}

extern "C" G_MODULE_EXPORT void
on_background_grey_button_clicked(GtkButton *button, gpointer user_data) {
   float gl = 0.2 + display_info_t::bg_col.x;
   if (gl > 1.0) gl = 0.1;
   display_info_t::bg_col = glm::vec3(gl, gl, gl);
   gtk_widget_queue_draw(display_info_t::gl_area);

}

extern "C" G_MODULE_EXPORT void
on_fog_togglebutton_toggled(GtkToggleButton *togglebutton,
                            gpointer         user_data) {

   if (gtk_toggle_button_get_active(togglebutton))
      set_draw_fog(true);
   else
      set_draw_fog(false);
}

extern "C" G_MODULE_EXPORT void
on_ao_togglebutton_toggled(GtkToggleButton *togglebutton,
                           gpointer         user_data) {

   if (gtk_toggle_button_get_active(togglebutton))
      set_draw_ao(true);
   else
      set_draw_ao(false);
}

extern "C" G_MODULE_EXPORT void
on_outline_togglebutton_toggled(GtkToggleButton *togglebutton,
                                gpointer         user_data) {

   if (gtk_toggle_button_get_active(togglebutton))
      display_info_t::do_outline = true;
   else
      display_info_t::do_outline = false;
   display_info_t::redraw();
}

 
extern "C" G_MODULE_EXPORT void
on_supersample_togglebutton_toggled(GtkToggleButton *togglebutton,
                                    gpointer         user_data) {

   display_info_t di;
   unsigned int w = di.get_width();
   unsigned int h = di.get_height();
   if (gtk_toggle_button_get_active(togglebutton))
      di.set_framebufffer_scale(2);
   else
      di.set_framebufffer_scale(1);

   // resize(GTK_GL_AREA(di.gl_area), w, h);

   // std::cout << "----- emit! " << std::endl;
   // g_signal_emit_by_name(G_OBJECT(di.main_application), "realise");

   di.redraw();
}


extern "C" G_MODULE_EXPORT void
on_quick_ribs_button_clicked(GtkButton *button, gpointer user_data) {
   quick_ribs();
   gtk_widget_queue_draw(display_info_t::gl_area);

}

extern "C" G_MODULE_EXPORT void
on_quick_surf_button_clicked(GtkButton *button, gpointer user_data) {
   quick_surf();
   gtk_widget_queue_draw(display_info_t::gl_area);

}


void
on_file_choose_file_activated(GtkFileChooser *chooser,
                              gpointer        user_data) {

   gchar *fn = gtk_file_chooser_get_filename(chooser);
   if (fn) {
      std::string file_name(fn);
      std::cout << "file chooser file activated!" << file_name << std::endl;
   }

}
