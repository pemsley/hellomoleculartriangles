
#include <vector>
#include <mmdb2/mmdb_manager.h>

namespace coot {
   std::vector<mmdb::Residue *>
   residues_near_residue(mmdb::Residue *res_ref,
                         mmdb::Manager *mol,
                         float radius);
}

